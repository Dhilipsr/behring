-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 17, 2021 at 05:56 AM
-- Server version: 5.7.34
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qsaccess_xifaxin_landing`
--

-- --------------------------------------------------------

--
-- Table structure for table `ctas`
--

CREATE TABLE `ctas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ctas`
--

INSERT INTO `ctas` (`id`, `title`, `slug`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Order Set Kit', 'order-set-kit', 'test', '2020-04-02 00:32:44', '2020-04-02 00:32:44'),
(2, 'Order XIFAXAN Plugin', 'order-xifaxan-plugin', 'test', '2020-04-02 00:33:03', '2020-04-02 00:33:03'),
(3, 'Ask GINA<sup>TM</sup>', 'ask-gina', 'https://www.research.net/r/plenvu-qsa', '2020-04-02 00:34:00', '2020-07-30 04:38:16'),
(4, 'Order XIFAXAN Plugin Now', 'order-xifaxan-plugin-now', 'test', '2020-04-02 00:34:59', '2020-04-02 00:34:59'),
(5, 'Order Set Kit', 'order-set-kit', 'test', '2020-04-02 00:35:16', '2020-04-02 00:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 5, 'section_one_content', 'rich_text_box', 'Section One Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(24, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(25, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(26, 5, 'section_two_content', 'rich_text_box', 'Section Two Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(27, 5, 'section_three_title', 'rich_text_box', 'Section Three Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(28, 5, 'section_four_content', 'rich_text_box', 'Section Four Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(29, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(30, 7, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(31, 7, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"slugify\":{\"origin\":\"title\"}}', 3),
(32, 7, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(33, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(34, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(35, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 9, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(37, 9, 'content_one', 'rich_text_box', 'Content One', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(38, 9, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(39, 9, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(40, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(41, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(42, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(43, 10, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 4),
(44, 10, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 5),
(45, 10, 'professional_title', 'text', 'Professional Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(46, 10, 'name_of_organization', 'text', 'Name Of Organization', 0, 1, 1, 1, 1, 1, '{}', 7),
(47, 10, 'organization_city', 'text', 'Organization City', 0, 1, 1, 1, 1, 1, '{}', 8),
(48, 10, 'organization_state', 'text', 'Organization State', 0, 1, 1, 1, 1, 1, '{}', 9),
(49, 10, 'type_of_organization', 'text', 'Type Of Organization', 0, 1, 1, 1, 1, 1, '{}', 10),
(50, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(51, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(52, 10, 'quick_support', 'text', 'Quick Support', 0, 1, 1, 1, 1, 1, '{}', 2),
(53, 10, 'order_set_kit', 'text', 'Order Set Kit', 0, 1, 1, 1, 1, 1, '{}', 3),
(54, 10, 'manager_name', 'text', 'Manager Name', 0, 1, 1, 1, 1, 1, '{}', 10);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-04-01 23:59:57', '2020-04-01 23:59:57'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-04-01 23:59:58', '2020-04-01 23:59:58'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-04-01 23:59:58', '2020-04-01 23:59:58'),
(5, 'home_pages', 'home-pages', 'Home Page', 'Home Pages', 'voyager-dot', 'App\\HomePage', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(7, 'ctas', 'ctas', 'Cta', 'Ctas', 'voyager-dot', 'App\\Cta', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(9, 'plugins', 'plugins', 'Plugin', 'Plugins', 'voyager-dot', 'App\\Plugin', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-02 00:30:34', '2020-04-29 05:20:06'),
(10, 'plugin_orders', 'plugin-orders', 'Plugin Order', 'Plugin Orders', 'voyager-dot', 'App\\PluginOrder', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-15 05:04:24', '2020-09-02 04:42:28');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_pages`
--

CREATE TABLE `home_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `section_one_content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `section_two_content` longtext COLLATE utf8mb4_unicode_ci,
  `section_three_title` longtext COLLATE utf8mb4_unicode_ci,
  `section_four_content` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_pages`
--

INSERT INTO `home_pages` (`id`, `section_one_content`, `created_at`, `updated_at`, `section_two_content`, `section_three_title`, `section_four_content`) VALUES
(1, '<p>Standardize Care for Your Patients <br />With Overt Hepatic Encephalopathy (OHE)</p>', '2020-04-02 00:25:00', '2020-06-23 13:44:45', '<p>Use the <strong>XIFAXAN<sup>&reg;</sup> Quick Support &amp; Access Plugin and the Order Set Kit</strong> to help standardize care within your organization. By standardizing care within your workflow, you can help identify and manage your patients living with OHE.</p>\r\n<p>With the XIFAXAN<sup>&reg;</sup> Quick Support &amp; Access Plugin, you can integrate important forms and resources into your organization\'s workflow and have these resources at your fingertips.</p>', '<p>XIFAXAN Has a Low Number Needed to Treat to Avoid OHE Recurrence and Related Hospitalization</p>', '<p>*A total of 4 patients would need to take XIFAXAN twice daily for 6 months to prevent 1 OHE episode.1<br />A total of 9 patients would need to take XIFAXAN twice daily for 6 months to prevent 1 HE-related hospitalization.1</p>\r\n<p>Reference: 1. Bass NM, Mullen KD, Sanyal A, et al. Rifaximin treatment in hepatic encephalopathy. <em>N Engl J Med. 2010</em>;362(12):1071-1081.</p>\r\n<p>With the XIFAXAN<sup>&reg;</sup> Quick Support &amp; Access Plugin, you can integrate all the important forms and resources into your organization\'s workflow and have everything at your fingertips</p>');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-04-01 23:59:59', '2020-04-01 23:59:59');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-04-02 00:00:00', '2020-04-02 00:00:00', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 9, '2020-04-02 00:00:00', '2020-04-02 00:36:46', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 5, 8, '2020-04-02 00:00:00', '2020-04-02 00:36:44', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 5, 6, '2020-04-02 00:00:00', '2020-04-02 00:36:40', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 2, '2020-04-02 00:00:00', '2020-04-02 00:36:46', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-04-02 00:00:00', '2020-04-02 00:35:43', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-04-02 00:00:01', '2020-04-02 00:35:43', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-04-02 00:00:01', '2020-04-02 00:35:43', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-04-02 00:00:01', '2020-04-02 00:35:43', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, 5, 7, '2020-04-02 00:00:01', '2020-04-02 00:36:41', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-04-02 00:00:05', '2020-04-02 00:35:43', 'voyager.hooks', NULL),
(12, 1, 'Home Pages', '', '_self', 'voyager-dot', NULL, 1, 1, '2020-04-02 00:15:05', '2020-04-02 00:36:14', 'voyager.home-pages.index', NULL),
(14, 1, 'Ctas', '', '_self', 'voyager-dot', NULL, 1, 2, '2020-04-02 00:23:42', '2020-04-02 00:36:32', 'voyager.ctas.index', NULL),
(16, 1, 'Plugins', '', '_self', 'voyager-dot', NULL, 1, 3, '2020-04-02 00:30:35', '2020-04-02 00:36:37', 'voyager.plugins.index', NULL),
(18, 1, 'Plugin Orders', '', '_self', 'voyager-dot', NULL, NULL, 3, '2020-04-15 05:04:25', '2020-04-15 05:04:25', 'voyager.plugin-orders.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_01_01_000000_add_voyager_user_fields', 1),
(3, '2016_01_01_000000_create_data_types_table', 1),
(4, '2016_05_19_173453_create_menu_table', 1),
(5, '2016_10_21_190000_create_roles_table', 1),
(6, '2016_10_21_190000_create_settings_table', 1),
(7, '2016_11_30_135954_create_permission_table', 1),
(8, '2016_11_30_141208_create_permission_role_table', 1),
(9, '2016_12_26_201236_data_types__add__server_side', 1),
(10, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(11, '2017_01_14_005015_create_translations_table', 1),
(12, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(13, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(14, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(15, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(16, '2017_08_05_000000_add_group_to_settings_table', 1),
(17, '2017_11_26_013050_add_user_role_relationship', 1),
(18, '2017_11_26_015000_create_user_roles_table', 1),
(19, '2018_03_11_000000_add_user_settings', 1),
(20, '2018_03_14_000000_add_details_to_data_types_table', 1),
(21, '2018_03_16_000000_make_settings_value_nullable', 1),
(22, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(2, 'browse_bread', NULL, '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(3, 'browse_database', NULL, '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(4, 'browse_media', NULL, '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(5, 'browse_compass', NULL, '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(6, 'browse_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(7, 'read_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(8, 'edit_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(9, 'add_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(10, 'delete_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(11, 'browse_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(12, 'read_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(13, 'edit_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(14, 'add_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(15, 'delete_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(16, 'browse_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(17, 'read_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(18, 'edit_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(19, 'add_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(20, 'delete_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(21, 'browse_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(22, 'read_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(23, 'edit_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(24, 'add_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(25, 'delete_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(26, 'browse_hooks', NULL, '2020-04-02 00:00:06', '2020-04-02 00:00:06'),
(27, 'browse_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(28, 'read_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(29, 'edit_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(30, 'add_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(31, 'delete_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(37, 'browse_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(38, 'read_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(39, 'edit_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(40, 'add_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(41, 'delete_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(47, 'browse_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(48, 'read_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(49, 'edit_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(50, 'add_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(51, 'delete_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(52, 'browse_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(53, 'read_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(54, 'edit_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(55, 'add_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(56, 'delete_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1);

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_one` longtext COLLATE utf8mb4_unicode_ci,
  `content_two` longtext COLLATE utf8mb4_unicode_ci,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `image`, `content_one`, `content_two`, `link`, `created_at`, `updated_at`) VALUES
(1, 'plugins/July2020/IEZw7R2NXoZTdPl3PGvs.jpg', '<p><strong>Help Your Patients Get the Support and Access to Their Prescribed Treatment</strong></p>\r\n<p>Give your patients and their caregivers evidence-based tools and resources to help them:</p>\r\n<ul>\r\n<li>Get financial support for their prescription</li>\r\n<li>Understand OHE risks and treatment options</li>\r\n<li>Set up medical alerts on digital devices</li>\r\n<li>GINA<sup>TM*</sup> (Guided INtegrated Assistance) can help you integrate the plugin into your EPIC EHR system</li>\r\n</ul>', '<p>Standardizing care is clearer and more efficient when you have the proper order set appear right in your EHR.<sup>1</sup> With a few keystrokes or mouse clicks you can be sure you\'re following your organization\'s guideline-driven protocols for OHE.</p>\r\n<p>The Order Set Kit has 6 components. See page 4 of the XIFAXAN QSA Leave-Behind for details.</p>', 'test', '2020-04-02 00:31:00', '2020-07-29 16:15:18');

-- --------------------------------------------------------

--
-- Table structure for table `plugin_orders`
--

CREATE TABLE `plugin_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `quick_support` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_set_kit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `professional_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_of_organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manager_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plugin_orders`
--

INSERT INTO `plugin_orders` (`id`, `quick_support`, `order_set_kit`, `name`, `email`, `professional_title`, `name_of_organization`, `organization_city`, `organization_state`, `manager_name`, `type_of_organization`, `created_at`, `updated_at`) VALUES
(23, 'yes', 'yes', 'Alisha Sarekari', 'alisha@gmail.com', 'Sr Accounts Manager', 'Godrej Infotech', 'Mumbai', 'Maharashtra', NULL, 'Large Group Practice or Physician Group', '2020-06-18 12:39:38', '2020-06-18 12:39:38'),
(24, 'yes', 'yes', 'K. Kurtz', 'kathleen.kurtz@franklynhc.com', 'TEST', 'Aventria', 'Parsippany', 'NJ', NULL, 'Medical Hospital', '2020-06-18 18:17:49', '2020-06-18 18:17:49'),
(25, 'yes', 'yes', 'test Collins', 'heather.collins@franklynhc.com', 'test', 'test', 'test', 'NJ', NULL, 'test the test field test', '2020-06-18 19:15:17', '2020-06-18 19:15:17'),
(26, 'yes', 'yes', 'Piyush Panchal', 'piyush.panchal@aventriahealth.com', 'DD', 'Aventria', 'Parsippany', 'NJ', NULL, NULL, '2020-06-23 03:06:58', '2020-06-23 03:06:58'),
(27, 'yes', 'yes', 'Kathleen', 'kathleen.kurtz@franklynhc.com', 'Director', 'Aventria Test', 'Parsippany', 'NJ', NULL, 'Health System', '2020-06-23 12:49:38', '2020-06-23 12:49:38'),
(28, 'yes', 'no', 'shawntay fernandez', 'smsydnor@houstonmethodist.org', 'Nurse manager', 'Houston Methodist Hospital', 'Houston', 'Texas', NULL, 'Health System', '2020-06-24 20:30:41', '2020-06-24 20:30:41'),
(29, 'yes', 'no', 'Sarah Mahdy', 'sarah.mahdy@walgreens.com', 'Pharmacist', 'Walgreens', 'Tulsa', 'Oklahoma', NULL, 'Health System', '2020-06-25 16:52:35', '2020-06-25 16:52:35'),
(30, 'yes', 'no', 'SHELBY NEAL', 's.neal@texomalivercenter.com', 'MEDICAL ASSISTANT', 'TEXOMA LIVER CENTER', 'DENISON', 'TX', NULL, 'Large Group Practice or Physician Group', '2020-06-25 17:01:27', '2020-06-25 17:01:27'),
(31, 'yes', 'no', 'Brian Henderson', 'brian.henderson@bauschhealth.com', 'DIA', 'Salix', 'Oklahoma City', 'OK', NULL, NULL, '2020-06-25 20:56:59', '2020-06-25 20:56:59'),
(32, 'yes', 'no', 'Brandi J Cobb', 'Bjcobb15@yahoo.com', 'RN', 'BAYLOR SCOTT AND WHITE ALL SAINTS', 'Ft worth', 'TX', NULL, 'Health System', '2020-06-26 18:10:38', '2020-06-26 18:10:38'),
(33, 'yes', 'yes', 'amy', 'ameyaleo9@gmail.com', 'doctor', 'hospital', 'newyork', 'states', 'mathew', 'clinic', '2020-06-29 12:05:53', '2020-06-29 12:05:53'),
(34, 'yes', 'no', 'Steven Gordon', 'steven.gordon@centrxhealth.com', 'Director of Pharmacy', 'UHS', 'Denison', 'Texas', 'Bob Malloy', 'Ambulatory Care Pharmacy', '2020-06-29 13:42:19', '2020-06-29 13:42:19'),
(35, 'yes', 'no', 'Cassidy Berns', 'pharlapwins@yahoo.com', 'Registered nurse', 'Integris', 'Oklahoma City', 'Oklahoma', 'Bob Malloy', 'Health System', '2020-06-29 17:39:26', '2020-06-29 17:39:26'),
(36, 'yes', 'no', 'Sam Lam', 'sllam@bidmc.harvard.edu', 'Clinical Pharmacist', 'BIDMC', 'Boston', 'MA', 'Erin Newman', 'Large Group Practice or Physician Group', '2020-06-30 12:27:14', '2020-06-30 12:27:14'),
(37, 'yes', 'yes', 'Madeline Pruitt', 'madeline.pruitt@salix.com', 'Manager', 'Salix', 'Tampa', 'Florida', NULL, NULL, '2020-07-07 14:25:36', '2020-07-07 14:25:36'),
(38, 'yes', 'no', 'Nicole Shelton', 'nicole.shelton@bswhealth.org', 'RN transplant coordinator', 'Baylor Scott and White All Saints Fort Worth', 'Fort Worth', 'TX', 'Robert Malloy', 'Health System', '2020-07-07 19:21:33', '2020-07-07 19:21:33'),
(39, 'yes', 'no', 'Arun Jesudian', 'abj9004@med.cornell.edu', 'Hepatologist', 'NYP-Weill Cornell Medicine', 'New York City', 'US', 'Arlene Tejada', 'Large Group Practice or Physician Group', '2020-07-08 17:57:45', '2020-07-08 17:57:45'),
(40, 'yes', 'yes', 'Xifaxan Support', 'qsaprogram@gmail.com', 'Owner', 'Quick Support and Access', 'Doylestown', 'Pennsylvania', NULL, NULL, '2020-07-08 19:53:37', '2020-07-08 19:53:37'),
(41, 'yes', 'no', 'Brooke Spino', 'brooke.spino@ahn.org', 'Pharm D', 'Forbes Regional Hospital', 'Monroeville', 'PA', 'Sam Baldigowski', 'Health System', '2020-07-09 15:36:29', '2020-07-09 15:36:29'),
(42, 'yes', 'no', 'Rob Gambrell', 'rob.gambrell@Salix.com', 'Director Strategic of Partnerships', 'Salix Pharmacuticals', 'Bridgewater', 'NJ', NULL, 'Large Group Practice or Physician Group', '2020-07-09 20:48:09', '2020-07-09 20:48:09'),
(43, 'yes', 'yes', 'Andres Riera', 'Andres.riera@towerhealth.org', 'MD', 'Tower Health', 'Reading', 'Pa', 'Peg Ridolfi', 'Large Group Practice or Physician Group', '2020-07-14 15:41:47', '2020-07-14 15:41:47'),
(44, 'yes', 'no', 'Jennifer Timan-Roman', 'Jennifer.Timan-Roman@UHhospitals.org', 'BSN, RN-BC', 'University Hospitals Cleveland', 'Cleveland', 'OH', 'Sam Baldigowski', 'Health System', '2020-07-15 14:24:56', '2020-07-15 14:24:56'),
(45, 'no', 'no', 'Pierre Gholam', 'Pierre.Gholam@UHhospitals.org', 'Medical Director, Liver Center of Excellence', 'University Hospitals Cleveland', 'Cleveland', 'OH', 'Sam Baldigowski', 'Health System', '2020-07-15 14:29:36', '2020-07-15 14:29:36'),
(46, 'yes', 'yes', 'Ramon Bataller', 'bataller@pitt.edu', 'Chief of Hepatology', 'University of Pittsburgh Medical Center', 'Pittsburgh', 'PA', 'Sam Baldigowski', 'Health System', '2020-07-15 14:35:09', '2020-07-15 14:35:09'),
(47, 'yes', 'yes', 'Naudia Jonassaint', 'jonassaintnl@upmc.edu', 'Clinical Director Hepatology', 'University of Pittsburgh Medical Center', 'Pittsburgh', 'Pennsylvania', 'Sam Baldigowski', 'Health System', '2020-07-15 15:10:39', '2020-07-15 15:10:39'),
(48, 'yes', 'no', 'Britta Staubes', 'britta.staubes@ochsner.org', 'Clinical Pharmacist', 'Ochsner Health', 'New Orleans', 'LA', 'Catherine Leach', 'Health System', '2020-07-15 19:39:06', '2020-07-15 19:39:06'),
(49, 'yes', 'no', 'Matthew Budke', 'mbudke@mercy.com', 'Lead Outpatient Pharmacist', 'Mercy Health West Hospital', 'Cincinnati', 'OH', 'Melissa Barger', 'Health System', '2020-07-23 17:16:54', '2020-07-23 17:16:54'),
(50, 'yes', 'no', 'Brian Henderson', 'brian.henderson@bauschhealth.com', 'Salix', 'salix', 'Oklahoma city', 'OK', 'Brian Henderson', NULL, '2020-07-23 20:17:39', '2020-07-23 20:17:39'),
(51, 'yes', 'yes', 'Eric Hoch', 'office@aventriahealth.com', 'VP', 'Aventria Health Group', 'Parsippany', 'NJ', 'Testing', 'Testing', '2020-07-25 11:45:39', '2020-07-25 11:45:39'),
(52, 'no', 'yes', 'Tamara Lenhoff', 'tamara.lenhoff@sfdph.org', 'Clinical Pharmacist', 'SFGH', 'San Francisco', 'CA', 'Marc Cua', 'Health System', '2020-07-28 18:10:35', '2020-07-28 18:10:35'),
(53, 'no', 'yes', 'Tamara Lenhoff', 'tamara.lenhoff@sfdph.org', 'Clinical Pharmacist', 'SFGH', 'San Francisco', 'CA', 'Marc Cua', 'Health System', '2020-07-28 18:10:36', '2020-07-28 18:10:36'),
(54, 'yes', 'no', 'Angela Smith', 'angela.smith@dignityhealth.org', 'Liver Transplant Coordinator', 'Saint Joseph\'s Hospital and Medical Center', 'Phoenix', 'Arizona', 'Kevin Akin', 'Health System', '2020-07-30 12:58:37', '2020-07-30 12:58:37'),
(55, 'yes', 'no', 'Patricia  Stine', 'stinepc@upmc.edu', 'RN Liver Transplant', 'University of Pittsburgh Medical Center', 'Pittsburgh', 'PA', 'Sam Baldigowski', 'Health System', '2020-07-30 19:08:17', '2020-07-30 19:08:17'),
(56, 'yes', 'no', 'Trent Larson', 'trent.larson@bannerhealth.com', 'Clinical Pharmacist', 'Banner Desert Medical Center', 'Mesa', 'Arizona', 'Kevin Akin', 'Health System', '2020-08-04 19:06:12', '2020-08-04 19:06:12'),
(57, 'yes', 'no', 'Tamara Lenhoff', 'tamara.lenhoff@sfdph.org', 'Clinical Pharmacist', 'SFGH', 'San Francisco', 'CA', 'Marc Cua', 'Health System', '2020-08-04 22:44:52', '2020-08-04 22:44:52'),
(58, 'yes', 'yes', 'Allison P Fleischmann', 'allison.fleischmann@salix.com', 'Executive Territory Manager', 'Bausch Health', 'Indianapolis', 'Indiana', 'Bradley White', NULL, '2020-08-10 14:23:20', '2020-08-10 14:23:20'),
(59, 'yes', 'yes', 'Allison P Fleischmann', 'allison.fleischmann@salix.com', 'Executive Territory Manager', 'Bausch Health', 'Indianapolis', 'Indiana', 'Bradey White', NULL, '2020-08-10 14:24:47', '2020-08-10 14:24:47'),
(60, 'yes', 'no', 'Judith O\'Brien', 'Judith.OBrien@AdventHealth.com', 'RPT', 'Advent Health', 'Orlando', 'FL', 'Madeline Pruitt', 'Health System', '2020-08-10 17:24:27', '2020-08-10 17:24:27'),
(61, 'yes', 'no', 'Nicole Girardi', 'ngirardi@bidmc.harvard.edu', 'Specialty Pharmacy Liaison', 'BIDMC Specialty Pharmacy', 'Norwood', 'Massachusetts', 'Erin Newman', 'Health System', '2020-08-11 17:01:12', '2020-08-11 17:01:12'),
(62, 'yes', 'no', 'ashleigh jones', 'ajones@mhg.com', 'Hospitalist Manager', 'Gulfport Memorial', 'Gulfport', 'MS', 'catherine Leach', 'Health System', '2020-08-12 18:43:08', '2020-08-12 18:43:08'),
(63, 'yes', 'no', 'Idalia Margarita Fernandez', 'maf9055@med.cornell.edu', 'Nurse Practitioner', 'NYP-Weill Cornell Medicine and NYP-Columbia', 'New York', 'New York', 'Arlene Tejada', 'Health System', '2020-08-12 19:56:12', '2020-08-12 19:56:12'),
(64, 'yes', 'no', 'Kripali Patel', 'kpatel12@bidmc.harvard.edu', 'Medication Discharge Program Coordinator', 'BIDMC Pharmacy', 'Boston', 'Massachusetts', 'Erin Newman', 'Health System', '2020-08-12 20:43:47', '2020-08-12 20:43:47'),
(65, 'yes', 'no', 'Kathy Shofner', 'kathy.shofner@clarkmemorial.org', 'Case Manager', 'clark memorial health', 'Jeffersonville, IN', 'IN', 'Melissa Barger', 'Care management - hospital', '2020-08-13 17:23:02', '2020-08-13 17:23:02'),
(66, 'yes', 'no', 'Megan Kunzig', 'Kunzigme@einstein.edu', 'Pharmacist', 'Einstein Medical Center', 'Philadelphia', 'Pa', 'Peg Ridolfi', 'Health System', '2020-08-14 14:14:00', '2020-08-14 14:14:00'),
(67, 'yes', 'no', 'Sonya Newsome', 'sbnewsome@umc.edu', 'RN', 'University of MS Medical Center', 'Jackson', 'MS', 'Catherine Leach', 'Health System', '2020-08-14 14:51:50', '2020-08-14 14:51:50'),
(68, 'yes', 'no', 'Tim Dudgeon', 'tim_dudgeon@trihealth.com', 'Pharmacist', 'Bethesda North Hospital - Trihealth', 'Cincinnati', 'KY', 'Melissa Barger', 'Health System', '2020-08-14 15:15:32', '2020-08-14 15:15:32'),
(69, 'yes', 'yes', 'Lauren kellerstrass', 'lastoll@saint-lukes.org', 'Supervisor of Pharmacy Reimbursement', 'St. Luke\'s Hospital', 'Kansas City', 'MO', 'Laura Frederes', 'Health System', '2020-08-14 16:07:36', '2020-08-14 16:07:36'),
(70, 'yes', 'no', 'Elin Cogan-adewunmi', 'elin.cogan-adewunmi@ssmhealth.com', 'Manager of Case Management', 'SSM Health SLU Hospital', 'St. Louis', 'MO', 'Laura Frederes', 'Health System', '2020-08-14 19:17:05', '2020-08-14 19:17:05'),
(71, 'yes', 'yes', 'Robert Brenner', 'Robert.Brenner@ahn.org', 'Pharm D, Dir of Meds to Beds program Allegheny Health Network', 'Allegheny General Hospital', 'Pittsburgh', 'PA', 'Sam Baldigowski', 'Health System', '2020-08-17 17:49:13', '2020-08-17 17:49:13'),
(72, 'yes', 'no', 'Philip Kong', 'philipkongpharmd@gmail.com', 'Pharmacist', 'University of California, San Francisco', 'San Francisco', 'California', 'Marc Cua', 'Health System', '2020-08-17 20:50:16', '2020-08-17 20:50:16'),
(73, 'yes', 'no', 'Melissa Pena', 'mpena@shieldsrx.com', 'Pharmacy Liaison', 'University Hospital', 'Newark', 'New Jersey', 'Arlene Tejada', 'Health System', '2020-08-18 14:59:51', '2020-08-18 14:59:51'),
(74, 'yes', 'no', 'Candace Buchanan', 'cbuchanan@phs.org', 'Care Manager', 'Presbyterian Hospital', 'Albuquerque', 'New Mexico', 'Kevin Akin', 'Health System', '2020-08-19 14:15:47', '2020-08-19 14:15:47'),
(75, 'yes', 'yes', 'Tracey Whipple', 'tracey.whipple@unitypoint.org', 'LPN', 'UnityPoint Health Des Moines - Center for Liver Disease', 'Des Moines', 'Iowa', 'Derek Lewis', 'Health System', '2020-08-19 16:25:21', '2020-08-19 16:25:21'),
(76, 'yes', 'no', 'Suzi Francis', 'suzi.francis@stelizabeth.com', 'Supervisor Ambulatory Pharmacy', 'St. Elizabeth', 'Edgewood', 'KY', 'Melissa Barger', 'Health System', '2020-08-19 19:06:02', '2020-08-19 19:06:02'),
(77, 'yes', 'no', 'Wendy Barry', 'wendy.barry@franklynhc.com', 'TEST', 'Aventria Health', 'Parsippany', 'New Jersey', NULL, 'Health System', '2020-08-21 17:02:32', '2020-08-21 17:02:32'),
(78, 'yes', 'yes', 'Jean Chandler-Robledo', 'Jean.chandler-robledo@sharp.com', 'Director of Case Management', 'Sharp Coronado', 'Coronado', 'CA', 'Rebecca OMarrah', 'Hospiral', '2020-08-21 19:20:20', '2020-08-21 19:20:20'),
(79, 'yes', 'yes', 'William Ford', 'William.ford@jefferson.edu', 'MD', 'Abington Jefferson', 'Abington', 'PA', 'Peg Ridolfi', 'Health System', '2020-08-25 19:29:30', '2020-08-25 19:29:30'),
(80, 'yes', 'no', 'Savant Mehta', 'mehtaliver@gmail.com', 'MD', 'GI Liver Group', 'Worcester', 'Massachusetts', 'Erin Newman', 'Large Group Practice or Physician Group', '2020-08-26 17:07:13', '2020-08-26 17:07:13'),
(81, 'yes', 'yes', 'William Lebak', 'william.lebak-ii@walgreens.com', 'Pharmacist', 'Our Lady of Lourdes', 'Camden', 'NJ', 'Peg Ridolfi', 'Health System', '2020-08-26 18:21:15', '2020-08-26 18:21:15'),
(82, 'yes', 'no', 'Patrick Horne', 'Patrick.Horne@medicine.ufl.edu', 'APRN', 'University of Florida Health', 'Gainesville', 'FL', 'Madeline Pruitt', 'Health System', '2020-08-28 12:50:30', '2020-08-28 12:50:30'),
(83, 'yes', 'no', 'Patrick Horne', 'Patrick.Horne@medicine.ufl.edu', 'APRN', 'University of Florida Health', 'Gainesville', 'FL', 'Madeline Pruitt', 'Health System', '2020-08-28 12:50:31', '2020-08-28 12:50:31'),
(84, 'yes', 'no', 'Brian Henderson', 'brian.henderson@bauschhealth.com', 'National Accounts', 'Salix', 'Oklahoma City', 'Oklahoma', 'Brian Henderson', 'Health System', '2020-09-01 14:21:37', '2020-09-01 14:21:37'),
(85, 'yes', 'yes', 'Sheila Pryer', 'Sheila.pryer@pennmedicine.upenn.edu', 'Lead Resource Coordinator', 'Lancaster General Hospital', 'Lancaster', 'PA', 'Peg Ridolfi', 'Health System', '2020-09-02 15:53:02', '2020-09-02 15:53:02'),
(86, 'yes', 'no', 'Louis Guzzi', 'bigtuna4@aol.com', 'MD', 'Advent Health Waterman', 'Tavares', 'FL', 'Madeline Pruitt', 'Health System', '2020-09-08 19:19:54', '2020-09-08 19:19:54'),
(87, 'yes', 'no', 'sanat', 'sanat.leo9@gmail.com', 'Sr Backend Engineer', 'Leo9 Studio', 'Mumbai', 'Maharashtra', 'Piyush Panchal', 'Large Group Practice or Physician Group', '2020-09-11 12:29:24', '2020-09-11 12:29:24'),
(88, 'yes', 'no', 'Diana Arnold', 'dianaarnold@texashealth.org', 'RN Care Manager', 'Hospital Medicine Associates', 'Fort Worth', 'TX', 'Bob Mallory', 'Large Group Practice or Physician Group', '2020-09-11 14:07:59', '2020-09-11 14:07:59'),
(89, 'yes', 'yes', 'Kristen Glaviano', 'kglaviano@gipartnersofil.com', 'Nurse', 'GI Solutions of Illinois', 'Chicago', 'IL', 'Jonathan Kuykendall', 'Large Group Practice or Physician Group', '2020-09-11 17:35:50', '2020-09-11 17:35:50'),
(90, 'yes', 'yes', 'Allison Fleischmann', 'allison.fleischmann@salix.com', 'Executive Territory Manager', 'Bausch Health', 'Indianapolis', 'Indiana', 'Allison Fleischmann', NULL, '2020-09-11 21:59:01', '2020-09-11 21:59:01'),
(91, 'yes', 'no', 'Danielle Patrick', 'danielle.patrick@salix.com', 'MD (Not a real one, I just play one on TV)', 'Bluff City Liver Experts', 'Memphis', 'TN', 'Myself', 'Large Group Practice or Physician Group', '2020-09-14 16:15:15', '2020-09-14 16:15:15'),
(92, 'yes', 'no', 'Charles McCormick', 'charles.mccormick@jefferson.edu', 'Pharmacist', 'Thomas Jefferson University Hospital', 'Philadelphia', 'PA', 'PegRidolfi', 'Health System', '2020-09-14 17:45:53', '2020-09-14 17:45:53'),
(93, 'yes', 'no', 'April Rodriguez', 'April.Rodriguez@BSWHealth.org', 'Registered Nurse', 'Baylor Liver Consultants', 'Dallas', 'TX', 'Brian Henderson/Robin Morgan', 'Large Group Practice or Physician Group', '2020-09-15 13:16:57', '2020-09-15 13:16:57'),
(94, 'yes', 'no', 'Darby Adams-Sikes', 'Darby.AdamsSikes@BSWHealth.org', 'Registered Nurse', 'Baylor Liver Consultants', 'Dallas', 'TX', 'Brian Henderson/Robin Morgan', 'Large Group Practice or Physician Group', '2020-09-15 13:18:12', '2020-09-15 13:18:12'),
(95, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock@VCUhealth.com', 'Social Worker', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:02:12', '2020-09-15 18:02:12'),
(96, 'yes', 'no', 'Jennifer Maldonado', 'Jennifer.Maldonado@VCUhealth.com', 'Social Worker', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:05:36', '2020-09-15 18:05:36'),
(97, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:09', '2020-09-15 18:15:09'),
(98, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:11', '2020-09-15 18:15:11'),
(99, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:14', '2020-09-15 18:15:14'),
(100, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:17', '2020-09-15 18:15:17'),
(101, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:18', '2020-09-15 18:15:18'),
(102, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:28', '2020-09-15 18:15:28'),
(103, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:28', '2020-09-15 18:15:28'),
(104, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:28', '2020-09-15 18:15:28'),
(105, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:28', '2020-09-15 18:15:28'),
(106, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:38', '2020-09-15 18:15:38'),
(107, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:39', '2020-09-15 18:15:39'),
(108, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:15:39', '2020-09-15 18:15:39'),
(109, 'yes', 'no', 'Sheila Bullock', 'Sheila.Bullock.@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:17:56', '2020-09-15 18:17:56'),
(110, 'yes', 'no', 'Sheila Bullock', 'sheila.bullock@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:19:08', '2020-09-15 18:19:08'),
(111, 'yes', 'no', 'Sheila Bullock', 'sheila.bullock@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:19:08', '2020-09-15 18:19:08'),
(112, 'yes', 'no', 'Sheila Bullock', 'sheila.bullock@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:19:08', '2020-09-15 18:19:08'),
(113, 'yes', 'no', 'Jennifer Maldonado', 'jennifer.maldonado@vcuhealth.org', 'MSW', 'VCU', 'Richmond', 'VA', 'Susan Baker', 'Health System', '2020-09-15 18:20:11', '2020-09-15 18:20:11'),
(114, 'yes', 'yes', 'Tracy Casey', 'thcasey@crmchealth.org', 'Office Manager', 'Cookeville Regional Medical Health Care', 'Cookeville', 'Tennessee', 'John Dees', 'Large Group Practice or Physician Group', '2020-09-15 19:19:31', '2020-09-15 19:19:31'),
(115, 'yes', 'no', 'Susan Beitzell', 'susan.beitzell@atlantagastro.com', 'Nurse Practitioner', 'Atlanta Gastro Athens', 'Athens', 'GA', 'Rachel Farley', 'Large Group Practice or Physician Group', '2020-09-16 02:07:55', '2020-09-16 02:07:55'),
(116, 'yes', 'yes', 'Courtney Matthys', 'courtneymatthys@icdlh.com', 'Manager', 'Illinois Center for Digestive and Liver Health', 'Barrington', 'IL', 'Jonathan Kuykendall', 'Large Group Practice or Physician Group', '2020-09-16 17:20:47', '2020-09-16 17:20:47'),
(117, 'yes', 'yes', 'Courtney Matthys', 'courtneymatthys@icdlh.com', 'Manager', 'Illinois Center for Digestive and Liver Health', 'Barrington', 'IL', 'Jonathan Kuykendall', 'Large Group Practice or Physician Group', '2020-09-16 17:20:48', '2020-09-16 17:20:48'),
(118, 'yes', 'yes', 'Heather Foster', 'heather.foster@salix.com', 'Territory Manager', 'Salix', 'Phoenix', 'AZ', 'Heather Foster', 'Health System', '2020-09-17 16:13:21', '2020-09-17 16:13:21'),
(119, 'yes', 'yes', 'Arturo Palacio', 'arturo.palacio@salix.com', 'Regional Sales Manager', 'Salix', 'Bridgewater', 'NJ', 'Arturo Palacio', 'Sales', '2020-09-17 18:14:41', '2020-09-17 18:14:41'),
(120, 'yes', 'no', 'Ashley Baxter', 'ashley_baxter@trihealth.com', 'Outpatient Pharmacy', 'Good Samaritan Hospital (Trihealth)', 'Cincinnatti', 'Ohio', 'Melissa Barger', 'Health System', '2020-09-22 15:09:05', '2020-09-22 15:09:05'),
(121, 'yes', 'no', 'Amber Strickland Bird', 'astrickland@athensgastro.com', 'Practice Manager', 'Athens Gastroenterology Association', 'Athens', 'Georgia', 'Erik', 'Large Group Practice or Physician Group', '2020-09-22 16:55:00', '2020-09-22 16:55:00'),
(122, 'yes', 'no', 'Donan Boggess', 'donan.boggess@walgreens.com', 'PharmD', 'Walgreens Specialty', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2020-09-22 18:55:30', '2020-09-22 18:55:30'),
(123, 'yes', 'no', 'Kate Flanagan', 'kateflanagan33@gmail.com', 'Nurse Practitioner', 'Greenwood Gastroenterology Center', 'Greenwood', 'Mississippi', 'John Dees', 'Large Group Practice or Physician Group', '2020-09-24 17:46:25', '2020-09-24 17:46:25'),
(124, 'yes', 'no', 'Shoshana Doga', 'sdoga@gidoc.org', 'Medical', 'Gastroenterologist Specialist Jefferson Torresdale', 'Philadelphia', 'Pa', 'Peg Ridolfi', 'Large Group Practice or Physician Group', '2020-09-24 19:52:20', '2020-09-24 19:52:20'),
(125, 'yes', 'no', 'Grace Jones', 'ngjones@uams.edu', 'Clinical Pharmacy Specialist', 'UAMS', 'Little Rock', 'AR', 'Catherine Leach', 'Health System', '2020-09-24 20:25:37', '2020-09-24 20:25:37'),
(126, 'yes', 'no', 'Grace Jones', 'ngjones@uams.edu', 'Clinical Pharmacy Specialist', 'UAMS', 'Little Rock', 'AR', 'Catherine Leach', 'Health System', '2020-09-24 20:25:37', '2020-09-24 20:25:37'),
(127, 'yes', 'no', 'Candace Metts', 'cmetts@keystonehealthcare.com', 'Patient Care Coodinator', 'Keystone Healthcare', 'Meridian', 'Mississippi', NULL, 'Health System', '2020-09-25 19:19:17', '2020-09-25 19:19:17'),
(128, 'yes', 'no', 'Alejandro robayo', 'alejandro.robayo@salix.com', 'Tm', 'Salix', 'Newark', 'New jersey', NULL, 'Health System', '2020-09-30 15:34:51', '2020-09-30 15:34:51'),
(129, 'yes', 'no', 'Brian Rajca', 'qlopez@aidh.com', 'MD', 'ASSOCIATES IN DIGESTIVE HEALTH', 'FORT MYERS', 'FORT MYERS FL', 'SCOTT', 'Large Group Practice or Physician Group', '2020-10-01 20:43:30', '2020-10-01 20:43:30'),
(130, 'no', 'no', 'Angela Schweers', 'AngelaRuth.Schweers@BSWHealth.org', 'Registered Nurse', 'Baylor Liver Consultants', 'Dallas', 'TX', 'Robin Morgan', 'Large Group Practice or Physician Group', '2020-10-02 02:22:05', '2020-10-02 02:22:05'),
(131, 'no', 'yes', 'brian rajca', 'qlopez@aidh.com', 'MD', 'ASSOCIATES IN DIGESTIVE HEALTH', 'FORT MYERS', 'FLORIDA', 'SCOTT', 'Large Group Practice or Physician Group', '2020-10-02 14:11:25', '2020-10-02 14:11:25'),
(132, 'yes', 'no', 'Melanie Proffitt', 'MelanieProffitt@mhd.com', 'Pharmacist', 'The Liver Institute - Methodist Hospital', 'Dallas', 'Texas', 'Robin Morgan', 'Large Group Practice or Physician Group', '2020-10-02 16:16:15', '2020-10-02 16:16:15'),
(133, 'yes', 'no', 'Noelle Hamzah', 'noelle.hamzah@bswhealth.org', 'Medical Assistant', 'Digestive Diseases Group', 'Plano', 'Texas', 'Everett Gibson', 'Large Group Practice or Physician Group', '2020-10-02 17:26:32', '2020-10-02 17:26:32'),
(134, 'yes', 'yes', 'Wen Cai', 'wcai@dccevv.com', 'Nurse Practitioner', 'Digestive Care Center', 'Evansville', 'Indiana', 'Melissa Barger', 'Large Group Practice or Physician Group', '2020-10-05 16:38:45', '2020-10-05 16:38:45'),
(135, 'yes', 'no', 'Delena Dinwiddie', 'ddinwiddie@mmclinic.com', 'Registered Nurse', 'Murfreesboro Medical Clinic', 'Murfreesboro', 'TN', 'John Dees', 'Large Group Practice or Physician Group', '2020-10-05 16:55:16', '2020-10-05 16:55:16'),
(136, 'yes', 'no', 'Connie Lindsey', 'cjlindse@utmck.edu', 'Practice Manager', 'UPA', 'Knoxville', 'TN', 'John Dees', 'Large Group Practice or Physician Group', '2020-10-05 16:57:40', '2020-10-05 16:57:40'),
(137, 'yes', 'no', 'Fallon Nicolosi', 'FallonNicolosi@mhd.com', 'Pharmacist', 'Methodist Hospital - Methodist Community Pharmacy', 'Dallas', 'Texas', 'Robin Morgan', 'Health System', '2020-10-05 21:57:07', '2020-10-05 21:57:07'),
(138, 'yes', 'no', 'Christina Cigolotti', 'christina_cigolotti@trihealth.com', 'RN - Care Management- Manager', 'Trihealth', 'Cincinnati', 'OH', 'Melissa Barger', 'Health System', '2020-10-07 16:35:11', '2020-10-07 16:35:11'),
(139, 'yes', 'no', 'Sam', 'info@texomalivercenter.com', 'Medical Assistant', 'Texoma Liver Center', 'Denison', 'Texas', 'Everett Gibson', 'Large Group Practice or Physician Group', '2020-10-07 17:50:31', '2020-10-07 17:50:31'),
(140, 'yes', 'no', 'Amanda Chaney', 'chaney.amanda@mayo.edu', 'APRN', 'Mayo Clinic', 'Jacksonville', 'FL', 'Madeline Pruitt', 'Health System', '2020-10-07 17:51:11', '2020-10-07 17:51:11'),
(141, 'yes', 'no', 'Gladys Navarro', 'gladys@livercenteroftexas.com', 'Medical Assistant', 'Liver Center of Texas', 'Dallas', 'Texas', 'Everett Gibson', 'Large Group Practice or Physician Group', '2020-10-08 15:11:50', '2020-10-08 15:11:50'),
(142, 'yes', 'yes', 'Mike Dalton', 'MIKEWDALTON@HOTMAIL.COM', 'Territory Manager', 'Bausch Health', 'TEMESCAL VALLEY', 'CA', NULL, NULL, '2020-10-08 15:33:41', '2020-10-08 15:33:41'),
(143, 'yes', 'no', 'Wendy Barry', 'wendy.barry@franklynhc.com', 'Manager', 'Aventria Health', 'Parsippany', 'New Jersey', 'No One', 'Large Group Practice or Physician Group', '2020-10-08 21:17:30', '2020-10-08 21:17:30'),
(144, 'no', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-09 08:47:39', '2020-10-09 08:47:39'),
(145, 'yes', 'yes', 'Carissa Tipper', 'carissa.tipper@hhsys.org', 'Certified Medical Assistant', 'Digestive Disease Center', 'Sheffield', 'Alabama', 'John Dees', 'Large Group Practice or Physician Group', '2020-10-09 15:03:23', '2020-10-09 15:03:23'),
(146, 'yes', 'no', 'Gladys Navarro', 'gladys@livercenteroftexas.com', 'Medical Assistant', 'Liver Center of Texas', 'Dallas', 'Texas', 'Everett Gibson', 'Large Group Practice or Physician Group', '2020-10-13 15:22:24', '2020-10-13 15:22:24'),
(147, 'no', 'yes', 'Herb Pettit', 'hpettit@bhsi.com', 'PharmD- Pharmacy Clinical Coordinator', 'Baptist Health Lexington', 'Lexington', 'KY', 'Melissa Barger', 'Health System', '2020-10-13 16:16:09', '2020-10-13 16:16:09'),
(148, 'yes', 'no', 'Xiomara', 'Delmira3@verizon.net', 'Social Worker', 'Montefiore Medical Center Moses Division Hospital', 'Bronx', 'NY', 'Arlene Tejada', 'Health System', '2020-10-15 13:49:46', '2020-10-15 13:49:46'),
(149, 'yes', 'yes', 'Benjamin P. Smith', 'Benjamin.Smith@promedica.org', 'Pharmacy Manager', 'Promedica OutPatient Pharmacy', 'Toledo', 'OH', 'Diana Higgins', 'Health System', '2020-10-16 19:10:24', '2020-10-16 19:10:24'),
(150, 'yes', 'yes', 'Suzy Kolk', 'suzannevkolk@texashealth.org', 'RN', 'Texas Health Care Digestive Care', 'Fort Worth', 'TX', 'Bob Malloy', 'Large Group Practice or Physician Group', '2020-10-16 20:36:09', '2020-10-16 20:36:09'),
(151, 'yes', 'yes', 'Tasha Ripley', 'natasha.ripley@unitypoint.org', 'Pharmacy Manager', 'Unity Point Health', 'Ankeny', 'Iowa', 'Laura Frederes', 'Health System', '2020-10-19 20:08:37', '2020-10-19 20:08:37'),
(152, 'no', 'yes', 'Nilesh Desai', 'nilesh.desai@bhsi.com', 'Chief Pharmacy Officer', 'Baptist Health', 'Louisville', 'KY', 'Melissa Barger', 'Health System', '2020-10-20 14:50:49', '2020-10-20 14:50:49'),
(153, 'no', 'yes', 'Suzi Francis', 'suzi.francis@stelizabeth.com', 'Pharm D, Supervisor of Ambulatory Clinical Pharmacy', 'St. Elizabeth Healthcare', 'Edgewood', 'KY', 'Melissa Barger', 'Health System', '2020-10-20 14:54:15', '2020-10-20 14:54:15'),
(154, 'yes', 'no', 'Marissa Cervera', 'marissa.cervera@uhs-sa.com', 'Pharmacist In Charge', 'University Health System Pavillion Pharmacy', 'San Antonio', 'Texas', 'Mike Bragg', 'Health System', '2020-10-20 20:56:23', '2020-10-20 20:56:23'),
(155, 'yes', 'yes', 'Davita Adanuvor', 'dadanuvor@salud.unm.edu', 'Nurse Practitioner', 'University of New Mexico', 'Albuquerque', 'New Mexico', 'Kevin Akin', 'Large Group Practice or Physician Group', '2020-10-21 23:51:06', '2020-10-21 23:51:06'),
(156, 'yes', 'no', 'KD', 'kdowell79@gmail.com', 'Test', 'Test', 'New York City', 'New York State', 'Korey Dowell', 'Health System', '2020-10-22 17:50:26', '2020-10-22 17:50:26'),
(157, 'yes', 'no', 'Celina Sharpe', 'Celina.sharpe@uhs-sa.com', 'Clinical Pharmacist', 'University Health System RBG Pharmacy', 'San Antonio', 'TX', 'Mike Bragg', 'Health System', '2020-10-22 17:52:25', '2020-10-22 17:52:25'),
(158, 'yes', 'yes', 'Chelsea Maier', 'chelmai@ulh.org', 'Pharm D, Specialty Pharmacy Manger', 'University of Louisville', 'Louisville', 'Kentucky', 'Melissa Barger', 'Health System', '2020-10-22 22:03:23', '2020-10-22 22:03:23'),
(159, 'yes', 'no', 'Brianna Guite', 'brianna.guite@trinityhealthofne.org', 'PharmD', 'Saint FrancisRx', 'Hartford', 'CT', 'Erin Newman', 'Large Group Practice or Physician Group', '2020-10-23 16:29:52', '2020-10-23 16:29:52'),
(160, 'yes', 'yes', 'Danielle Vance', 'dvance@smgnj.com', 'RN', 'Summit Medical Group', 'Florham Park', 'NJ', 'Wendy Alton', 'Large Group Practice or Physician Group', '2020-10-27 14:32:11', '2020-10-27 14:32:11'),
(161, 'yes', 'no', 'Tu Tran', 'kennygg32@yahoo.com', 'MD', 'Paramount  medical specialty group', 'Huntington park', 'California', NULL, 'Large Group Practice or Physician Group', '2020-10-27 19:16:25', '2020-10-27 19:16:25'),
(162, 'yes', 'yes', 'kiara', 'kiaragossett@yahoo.com', 'Medical Assistant', 'Digestive Associates of Ohio', 'Columbus', 'Ohio', 'Dianna Higgins', 'Health System', '2020-10-28 16:51:43', '2020-10-28 16:51:43'),
(163, 'yes', 'no', 'Kimberly Forde', 'kimberly.forde@tuhs.temple.edu', 'MD', 'Temple University Hospital', 'Philadelphia', 'Pa', 'Peg Ridolfi', 'Health System', '2020-10-28 20:53:40', '2020-10-28 20:53:40'),
(164, 'yes', 'no', 'Stephanie Jones', 'jonessm1@uhnj.org', 'Pharmacy Liason', 'Rutgers', 'Newark', 'New Jersey', 'Alejandro Robayo', 'Health System', '2020-10-30 16:23:21', '2020-10-30 16:23:21'),
(165, 'yes', 'yes', 'Debi tutor', 'debi52t@aol.com', 'Ma', 'Providence gastro', 'Southfield', 'Mi', 'Dianna higgins', 'Health System', '2020-11-02 16:53:18', '2020-11-02 16:53:18'),
(166, 'yes', 'yes', 'Debi tutor', 'debi52t@aol.com', 'Ma', 'Providence gastro', 'Southfield', 'Mi', 'Dianna higgins', 'Health System', '2020-11-02 16:53:18', '2020-11-02 16:53:18'),
(167, 'yes', 'yes', 'Yerelyn Fernandez', 'Fernandye@sjhmc.org', 'Office Manager', 'Saint joseph', 'Hoboken', 'New Jersey', 'Alejandro Robayo', 'Health System', '2020-11-02 20:13:51', '2020-11-02 20:13:51'),
(168, 'yes', 'no', 'Melissa', 'Melissa@gastroassoc.org', 'LPN', 'Gastroenterology Associaties of Chattanooga', 'Chattanooga', 'TN', 'Karen Burch', 'Large Group Practice or Physician Group', '2020-11-05 18:00:50', '2020-11-05 18:00:50'),
(169, 'no', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-07 22:38:46', '2020-11-07 22:38:46'),
(170, 'no', 'yes', 'Scott Ritchey', 'SCOTT.RITCHEY@bauschhealth.com', 'Regional Account Executive', 'Bausch Health Companies', 'WINTER SPRINGS', 'FL', 'Scott Ritchey', NULL, '2020-11-09 17:32:20', '2020-11-09 17:32:20'),
(171, 'yes', 'no', 'Patrick Leinauer', 'patrick.leinauer@ascension.org', 'Sr. Director, Ascension Patient Medication Assistance Program', 'Ascension', 'St. Louis', 'Missouri', 'Brian Henderson', 'Health System', '2020-11-11 17:01:58', '2020-11-11 17:01:58'),
(172, 'yes', 'no', 'Jennier Noble', 'jennifer.noble@ogdenclinic.com', 'Medical Assistant', 'Ogden GI Clinic', 'Ogden', 'Utah', NULL, 'Large Group Practice or Physician Group', '2020-11-13 19:33:10', '2020-11-13 19:33:10'),
(173, 'yes', 'no', 'Wendy Bigelow', 'wendy.bigelow@fmolhs.org', 'LPN', 'Saint Francis Medical Center', 'Monroe', 'LA', 'Catherine Leach', 'Health System', '2020-11-19 16:15:02', '2020-11-19 16:15:02'),
(174, 'yes', 'no', 'Maria Yi', 'rxm.13641@store.walgreens.com', 'Pharmacy Manager', 'Walgreens Pharmacy Co.', 'Atlanta', 'Georga', 'Matthew Carter', 'Health System', '2020-11-20 12:14:24', '2020-11-20 12:14:24'),
(175, 'yes', 'yes', 'Stephen Wang', 'Stephen.wang@salix.com', 'Regional Sales Manager', 'Salix', 'Randolph', 'NJ', NULL, 'Large Group Practice or Physician Group', '2020-11-20 20:56:50', '2020-11-20 20:56:50'),
(176, 'yes', 'no', 'Joyce Hammonds', 'joyce.hammonds@uchealth.com', 'Pharmacy - Medical Access Coordinator', 'University of Cincinnati', 'Cincinnati', 'OH', 'Melissa Barger', 'Health System', '2020-11-25 21:50:09', '2020-11-25 21:50:09'),
(177, 'yes', 'yes', 'Julio Gutierrez', 'juliogutierrez@gmail.com', 'MD', 'SCRIPPS', 'La Jolla', 'Ca', 'Rebecca OMarrah', 'Health System', '2020-11-29 17:49:54', '2020-11-29 17:49:54'),
(178, 'yes', 'no', 'Ivonne Ugalde', 'iugalde@huntregional.org', 'Medical Assistant', 'Hunt Regional', 'Greenville', 'Texas', 'Everett Gibson', 'Health System', '2020-12-01 20:34:42', '2020-12-01 20:34:42'),
(179, 'no', 'yes', 'Suzie McDowell', 'suziemcdowell3@gmail.com', 'RN', 'University of Washington', 'Seattle', 'WA', 'Josh Bingham', 'Health System', '2020-12-02 15:44:19', '2020-12-02 15:44:19'),
(180, 'yes', 'no', 'Mary Sarah Karami', 'mary.karami@walgreens.com', 'Pharmacist', 'Health System', 'Tulsa', 'Oklahoma', 'Bob Malloy', 'Health System', '2020-12-02 20:50:50', '2020-12-02 20:50:50'),
(181, 'yes', 'no', 'Jose Cruz', 'jesus.cruz@memorialhremann.org', 'Certified Pharmacy Technician', 'Memorial Hermann', 'Houston', 'Texaas', 'Brian Henderson', 'Health System', '2020-12-03 16:44:32', '2020-12-03 16:44:32'),
(182, 'no', 'yes', 'Jennifer Vincent, MD', 'Jennifer.Vincent@BSWHealth.org', 'Chief of Hepatology', 'Baylor Scott and White', 'Temple', 'Texas', 'Brian Henderson', 'Health System', '2020-12-03 21:22:50', '2020-12-03 21:22:50'),
(183, 'yes', 'no', 'Martin Moehlen, MD', 'mmoehle@tulane.edu', 'Director, Gastroenterology & Hepatology Research', 'Tulane Medical Center', 'New Orleans', 'Louisiana', 'Catherine Leach', 'Health System', '2020-12-04 18:31:11', '2020-12-04 18:31:11'),
(184, 'yes', 'no', 'Mehdi Khorsandi', 'gabby@mehdikhorsandimd.com', 'MD', 'Mehdi Khorsandi MD', 'Glendale', 'CA', 'Tara Scheibel', 'Large Group Practice or Physician Group', '2020-12-07 19:44:14', '2020-12-07 19:44:14'),
(185, 'yes', 'no', 'Marie Lugtu', 'mglugtu@ucdavis.edu', 'CPhT', 'UC Davis', 'Sacramento', 'CA', 'Marc Cua', 'Health System', '2020-12-08 15:47:45', '2020-12-08 15:47:45'),
(186, 'yes', 'no', 'Mariecel Gregory', 'mgregory@ucdavis.edu', 'PharmacyTechnician III', 'UC Davis Medical', 'Sacramento', 'Ca', 'Marc Cua', 'Health System', '2020-12-08 20:06:18', '2020-12-08 20:06:18'),
(187, 'yes', 'no', 'Erika', 'Eharrington@metrohealth.org', 'Pharm D', 'MetroHealth', 'Cleveland', 'OH', 'Sam Baldigowski', 'Health System', '2020-12-11 20:54:49', '2020-12-11 20:54:49'),
(188, 'yes', 'yes', 'Katie Coker', 'katiecoker22@gmail.com', 'Transplant Social Worker', 'Baylor University Medical Center', 'Dallas', 'Texas', 'Bob Malloy', 'Health System', '2020-12-14 21:47:59', '2020-12-14 21:47:59'),
(189, 'yes', 'no', 'Margaret Andrejewski', 'andrejewskim@upmc.edu', 'PA', 'UPMC', 'Pittsburgh', 'Pennsylvania', 'Sam Baldigowski', 'Health System', '2020-12-15 14:28:00', '2020-12-15 14:28:00'),
(190, 'yes', 'yes', 'Carol Zinya', 'carolgozinya@gmail.com', 'Caseworker', 'Munson', 'Traverse City', 'Michigan', 'Diana Higgins', 'Health System', '2020-12-16 23:43:11', '2020-12-16 23:43:11'),
(191, 'no', 'no', 'Michel Mendler', 'Mmendler@ucsd.edu', 'MD', 'UCSD', 'San Diego', 'Ca', 'Rebecca OMarrah', 'Health System', '2020-12-17 18:02:53', '2020-12-17 18:02:53'),
(192, 'yes', 'yes', 'Anesia Reticker', 'Anesia.reticker@uchospitals.edu', 'Clinical Pharmacist', 'University of Chicago', 'Chicago', 'IL', 'Che King', 'Health System', '2020-12-17 19:52:38', '2020-12-17 19:52:38'),
(193, 'yes', 'yes', 'Nancy Reau', 'nancy_reau@rush.edu', 'Transplant Hepatologist', 'University Hepatologists', 'Bourbonnais', 'IL', 'Che King', 'Large Group Practice or Physician Group', '2020-12-18 14:51:39', '2020-12-18 14:51:39'),
(194, 'yes', 'no', 'Sara bender', 'sara.bender@hotmail.com', 'RN', 'Davis', 'Layton', 'Ut', 'Josh Bingham', NULL, '2020-12-18 17:48:35', '2020-12-18 17:48:35'),
(195, 'yes', 'no', 'Zach Battle', 'zach.battle@mercy.com', 'CPht', 'Mercy Health Fairfield', 'Fairfield', 'OH', 'Melissa Barger', 'Health System', '2020-12-24 02:51:54', '2020-12-24 02:51:54'),
(196, 'yes', 'no', 'Nikita Young', 'Nikita.young@emoryhealthcare.org', 'RN', 'Emory', 'Atlanta', 'GA', 'Rachel Farley', 'Large Group Practice or Physician Group', '2020-12-31 02:10:37', '2020-12-31 02:10:37'),
(197, 'yes', 'no', 'Hope Blackshear', 'suzannah.blackshear@northside.com', 'LMSW', 'Northside', 'Lawrenceville', 'GA', 'Rachel Farley', 'Health System', '2020-12-31 02:13:51', '2020-12-31 02:13:51'),
(198, 'yes', 'no', 'Tara Koehler', 'tkoehler@augusta.org', 'RPH', 'Augusta University', 'Augusta', 'GA', 'Rachel Farley', 'Health System', '2020-12-31 02:18:21', '2020-12-31 02:18:21'),
(199, 'no', 'yes', 'Jaime Ackerman', 'william.garcia@atlantagastro.com', 'Physician Assistant', 'Atlanta Gastroenterology', 'Marietta', 'Georgia', 'Sahl', 'Large Group Practice or Physician Group', '2021-01-05 12:53:02', '2021-01-05 12:53:02'),
(200, 'yes', 'yes', 'Brian van der Linden, MD', 'Amanda.Sigmon@hcahealthcare.com', 'MD', 'Lewis Gale Physicians', 'Salem', 'Virginia', 'Tony Cassell', 'Health System', '2021-01-05 20:37:00', '2021-01-05 20:37:00'),
(201, 'yes', 'no', 'Rachel Pennington', 'Rachel.Pennington@memorialhermann.org', 'Specialty Program Technician', 'Memorial Hermann', 'Katy', 'Texas', 'Brian Henderson', 'Health System', '2021-01-06 15:54:42', '2021-01-06 15:54:42'),
(202, 'no', 'yes', 'joyce peji', 'alexis.swinney@atlantagastro.com', 'registered medical assistant', 'atlanta gastroenterology associates', 'atlanta', 'GA', 'Sahl Shaheed', 'Large Group Practice or Physician Group', '2021-01-06 17:12:36', '2021-01-06 17:12:36'),
(203, 'no', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-11 04:06:39', '2021-01-11 04:06:39'),
(204, 'yes', 'yes', 'Mark Gagnon', 'mark.gagnon@ascension.org', 'Director of Pharmacy, PharmD', 'Ascension Via Christi', 'Wichita', 'Kansas', 'Laura Skarperud', 'Health System', '2021-01-12 17:54:01', '2021-01-12 17:54:01'),
(205, 'yes', 'no', 'Randy Eaton', 'reaton@washgi.com', 'Data & Quality Analyst', 'Washington Gastroenterology', 'Tacoma', 'WA', NULL, 'Large Group Practice or Physician Group', '2021-01-12 18:15:31', '2021-01-12 18:15:31'),
(206, 'no', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-14 22:34:06', '2021-01-14 22:34:06'),
(207, 'no', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 06:09:56', '2021-01-16 06:09:56'),
(208, 'yes', 'yes', 'Gerald Simons', 'pasimons@morrisonhealth.com', 'PA', 'Morrison Health', 'Water Mill', 'NY', 'Vinnie 516-729-8608', 'Large Group Practice or Physician Group', '2021-01-19 15:32:24', '2021-01-19 15:32:24'),
(209, 'yes', 'yes', 'Liz Stewart', 'lstewart@pinnaclehc.com', 'Sr. Director', 'Pinnacle Health Comm div of Aventria', 'Doylestown', 'Pa', NULL, NULL, '2021-01-27 16:33:30', '2021-01-27 16:33:30'),
(210, 'yes', 'no', 'Stephanie Fajardogome', 'sfajardogome@iuhealth.org', 'MA', 'Indiana University - DaLD clinic', 'Indianapolis', 'Indiana', 'Melissa Barger', 'Large Group Practice or Physician Group', '2021-01-29 20:06:31', '2021-01-29 20:06:31'),
(211, 'yes', 'no', 'Suzy Kolk', 'suzannevkolk@texashealth.org', 'Texas', 'Texas Health Care Digestive Care', 'Fort Worth', 'TX', 'Robert Malloy', 'Large Group Practice or Physician Group', '2021-02-01 19:33:40', '2021-02-01 19:33:40'),
(212, 'yes', 'no', 'Thomas Swantkowski', 'tswantkowski@gmail.com', 'MD', 'Pinehurst Medical Clinic', 'Pinehurst', 'NC', NULL, 'Large Group Practice or Physician Group', '2021-02-03 15:29:02', '2021-02-03 15:29:02'),
(213, 'yes', 'no', 'Thomas Swantkowski', 'tswantkowski@gmail.com', 'MD', 'Pinehurst Medical Clinic', 'Pinehurst', 'NC', NULL, 'Large Group Practice or Physician Group', '2021-02-03 15:29:02', '2021-02-03 15:29:02'),
(214, 'yes', 'yes', 'Bradley A. Connor, MD', 'marina.rogova@connormd.com', 'MD', 'Bradley A. Connor, MD, PLLC', 'New York', 'NY', NULL, NULL, '2021-02-03 16:16:58', '2021-02-03 16:16:58'),
(215, 'yes', 'yes', 'Ashley Siehl', 'ashleysiehl@yahoo.com', 'Medical Assistant', 'Advanced Digestive Care', 'Annandale', 'VA', 'Maryam', 'Large Group Practice or Physician Group', '2021-02-03 17:46:07', '2021-02-03 17:46:07'),
(216, 'yes', 'yes', 'Sarah', 'sarah.cslcenter@gmail.com', 'Lpn', 'Colon stomach and liver center', 'Lansdowne', 'Va', 'Maryam', 'Large Group Practice or Physician Group', '2021-02-04 17:45:13', '2021-02-04 17:45:13'),
(217, 'yes', 'yes', 'Sarah', 'sarah.cslcenter@gmail.com', 'Lpn', 'Colon stomach and liver center', 'Lansdowne', 'Va', 'Maryam', 'Large Group Practice or Physician Group', '2021-02-04 17:45:14', '2021-02-04 17:45:14'),
(218, 'yes', 'yes', 'Sarah', 'sarah.cslcenter@gmail.com', 'Lpn', 'Colon stomach and liver center', 'Lansdowne', 'Va', 'Maryam', 'Large Group Practice or Physician Group', '2021-02-04 17:45:14', '2021-02-04 17:45:14'),
(219, 'yes', 'no', 'Marissa Garza', 'marissa.garza@uhs-sa.com', 'Reimbursement', 'University Health System', 'San Antonio', 'Texas', 'Mike Bragg', 'Health System', '2021-02-09 21:38:39', '2021-02-09 21:38:39'),
(220, 'yes', 'no', 'Tasha Burgess', 'tburgess10@umphysicians.umn.edu', 'Nurse', 'Mhealth Fairview Hepatology', 'Minneapolis', 'minnesota', NULL, 'Large Group Practice or Physician Group', '2021-02-10 19:22:40', '2021-02-10 19:22:40'),
(221, 'yes', 'no', 'Umesh Choudhry', 'jbassett@gastrofl.com', 'MD. FACG', 'Advanced Digestive Care P A: Gastro Florida', 'Clearwater Florida', 'Florida', NULL, 'Large Group Practice or Physician Group', '2021-02-17 04:45:02', '2021-02-17 04:45:02'),
(222, 'yes', 'yes', 'Stacey Kale', 'staceykale@cox.net', 'DNP', 'Iola Derm 101', 'Iola', 'KS', NULL, 'Large Group Practice or Physician Group', '2021-02-23 15:50:54', '2021-02-23 15:50:54'),
(223, 'yes', 'no', 'Reva Mcmillan Ford', 'reva.mcmillanford@bswhealth.org', 'LMSW', 'Baylor Scott & White', 'Dallas', 'Texas', 'Robert Malloy', 'Health System', '2021-02-23 19:00:50', '2021-02-23 19:00:50'),
(224, 'yes', 'no', 'Jamie', 'RXM.16198@store.walgreens.com', 'Pharm', 'Crozer Chester Medical Center', 'Chester', 'Pa', 'Peg Ridolfi', 'Health System', '2021-02-24 19:43:47', '2021-02-24 19:43:47'),
(225, 'yes', 'yes', 'Shaun Lancon', 'shaunrlancon@gmail.com', 'Clinical Assistant', 'Gastroenterology Associates', 'Manassas', 'VA', 'Maryam Webb', 'Large Group Practice or Physician Group', '2021-02-24 20:06:35', '2021-02-24 20:06:35'),
(226, 'no', 'yes', 'Hannah Tate', 'hannah.tate@franciscanalliance.org', 'RN', 'Franciscan Health Indianapolis', 'Indianapolis', 'IN', 'Melissa Barger', 'Health System', '2021-03-01 16:12:59', '2021-03-01 16:12:59'),
(227, 'yes', 'no', 'Phillip McCreary', 'phillip.w.mccreary@vumc.org', 'Clinical Pharmacist', 'Vanderbilt', 'Nashville', 'Tennessee', 'John Dees', 'Health System', '2021-03-03 14:58:40', '2021-03-03 14:58:40'),
(228, 'yes', 'yes', 'Christa Edwards', 'cedwards@mmclinic.com', 'Medical Assistant', 'MMC', 'Murfreesboro', 'TN', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 17:26:12', '2021-03-03 17:26:12'),
(229, 'yes', 'yes', 'Christa Edwards', 'cedwards@mmclinic.com', 'Medical Assistant', 'MMC', 'Murfreesboro', 'TN', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 17:26:12', '2021-03-03 17:26:12'),
(230, 'yes', 'yes', 'Aimee Warren', 'awarren@mmclinic.com', 'Medical Assistant', 'MMC', 'Murfreesboro', 'TN', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 18:26:20', '2021-03-03 18:26:20'),
(231, 'no', 'yes', 'martin porcelli,d.o.', 'ppomando1@msn.com', 'osteopathic physician', 'dr porcelli', 'pomona', 'ca', 'unknown', 'Large Group Practice or Physician Group', '2021-03-03 18:50:25', '2021-03-03 18:50:25'),
(232, 'yes', 'yes', 'Mary Romano', 'mromano@illinoisgastro.com', 'Office Manager', 'Illinois Gastro Group', 'Arlington Heights', 'Illinois', 'Jonathan Kuykendall', 'Large Group Practice or Physician Group', '2021-03-03 19:15:16', '2021-03-03 19:15:16'),
(233, 'yes', 'yes', 'Meredith Turner', 'meredith.turner@hcahealthcare.com', 'Medical Assistant', 'Tri Star Gastroenterology Specialties of Middle TN', 'Smyrna', 'TN', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 22:11:31', '2021-03-03 22:11:31'),
(234, 'yes', 'yes', 'Brittany Sley', 'brisley@bgapc.com', 'Medical Assistant', 'BGA', 'Birmingham', 'Alabama', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 22:23:28', '2021-03-03 22:23:28'),
(235, 'yes', 'yes', 'Jessica Abercrombie', 'jabercrombie@bgapc.com', 'Medical Assistant', 'BGA', 'Birmingham', 'Alabama', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 22:27:49', '2021-03-03 22:27:49'),
(236, 'yes', 'yes', 'Jessica Abercrombie', 'jabercrombie@bgapc.com', 'Medical Assistant', 'BGA', 'Birmingham', 'Alabama', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 22:27:49', '2021-03-03 22:27:49'),
(237, 'yes', 'yes', 'Jessica Abercrombie', 'jabercrombie@bgapc.com', 'Medical Assistant', 'BGA', 'Birmingham', 'Alabama', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 22:27:49', '2021-03-03 22:27:49'),
(238, 'yes', 'yes', 'Hope Pendelton', 'hpendleton@bgapc.com', 'Medical Assistant', 'BGA', 'Birmingham', 'Alabama', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-03 22:30:56', '2021-03-03 22:30:56'),
(239, 'yes', 'no', 'Marlon Villacorta', 'mvillacorta@tuftsmedicalcenter.org', 'Clinical Resource Manager', 'Tufts Medical Center', 'Boston', 'MA', 'Erin Newman', 'Large Group Practice or Physician Group', '2021-03-04 15:06:29', '2021-03-04 15:06:29'),
(240, 'yes', 'no', 'Tricia Caron', 'tricia.caron@hhchealth.org', 'APRN Hartford HealthCare', 'Hartford Hospital', 'Hartford', 'CT', 'Erin Newman', 'Large Group Practice or Physician Group', '2021-03-04 15:08:42', '2021-03-04 15:08:42'),
(241, 'yes', 'no', 'Roxanne Arkoette', 'Roxanna.Arkoette@baystatehealth.org', 'Medical Assistant', 'Baystate Health', 'Springfield', 'MA', 'Erin Newman', 'Large Group Practice or Physician Group', '2021-03-04 15:11:02', '2021-03-04 15:11:02'),
(242, 'no', 'yes', 'Tiffany LaDow', 'Tiffany.LaDow@BSWHealth.org', 'PharmD', 'Baylor Scott & White Medical Center - Temple', 'Temple', 'Texas', 'Mike Bragg', 'Health System', '2021-03-04 19:20:38', '2021-03-04 19:20:38'),
(243, 'yes', 'no', 'Blessy Kunjachen', 'blessy.kunjachen@utsoythwestern.edu', 'RN-Care Coordinator', 'UT Southwestern', 'Dallas', 'Tx', 'Bob  Malloy', 'Health System', '2021-03-05 18:00:47', '2021-03-05 18:00:47'),
(244, 'yes', 'no', 'Blessy Kunjachen', 'blessy.kunjachen@utsouthwestern.edu', 'RN Care Coordinator', 'UTSW-Clements University Hospital', 'Dallas', 'Tx', 'Bob Malloy', 'Health System', '2021-03-05 18:28:03', '2021-03-05 18:28:03'),
(245, 'yes', 'no', 'Jean-Marc Berard', 'jean-marc.berard@salix.com', 'Territory Manager - Salix', 'Salix', 'Hartford', 'CT', 'Jean-Marc Berard', 'Large Group Practice or Physician Group', '2021-03-11 16:47:19', '2021-03-11 16:47:19'),
(246, 'yes', 'no', 'Jean-Marc Berard', 'jean-marc.berard@salix.com', 'Territory Manager - Salix', 'Salix', 'Hartford', 'CT', 'Jean-Marc Berard', 'Large Group Practice or Physician Group', '2021-03-11 16:47:19', '2021-03-11 16:47:19'),
(247, 'yes', 'no', 'Dr. Krunal Patel', 'Krunal.Patel3@umassmemorial.org', 'MD', 'UMass Memorial Health Care', 'Worcester', 'MA', 'Erin Newman', 'Large Group Practice or Physician Group', '2021-03-15 13:18:41', '2021-03-15 13:18:41'),
(248, 'yes', 'no', 'Cassidy Berns', 'cassidy.berns@integrisok.com', 'RN', 'IBMC', 'OKC', 'OK', 'Bob Malloy', 'Health System', '2021-03-16 15:43:53', '2021-03-16 15:43:53'),
(249, 'yes', 'yes', 'J Bridgforth', 'jbridgeforth@jsmc.org', 'Nurse Practitioner', 'Jennie Stuart Gastroenteroolgy', 'Hopkinsville', 'KY', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-17 03:26:59', '2021-03-17 03:26:59'),
(250, 'yes', 'yes', 'Jennifer  Dayton', 'jdayton@jsmc.org', 'Registered Nurse', 'Jennie Stuart Gastroenterology', 'Hopkinsville', 'KY', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-17 03:28:36', '2021-03-17 03:28:36'),
(251, 'yes', 'yes', 'Stephanie Combs', 'srcombs@jsmc.org', 'Registered Nurse', 'Jennie Stuart Gastroenterology', 'Hopkinsville', 'KY', 'John Dees', 'Large Group Practice or Physician Group', '2021-03-17 03:29:58', '2021-03-17 03:29:58'),
(252, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:26', '2021-03-17 14:07:26'),
(253, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:28', '2021-03-17 14:07:28'),
(254, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:29', '2021-03-17 14:07:29'),
(255, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:37', '2021-03-17 14:07:37'),
(256, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:38', '2021-03-17 14:07:38'),
(257, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:42', '2021-03-17 14:07:42');
INSERT INTO `plugin_orders` (`id`, `quick_support`, `order_set_kit`, `name`, `email`, `professional_title`, `name_of_organization`, `organization_city`, `organization_state`, `manager_name`, `type_of_organization`, `created_at`, `updated_at`) VALUES
(258, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:43', '2021-03-17 14:07:43'),
(259, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:45', '2021-03-17 14:07:45'),
(260, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:07:49', '2021-03-17 14:07:49'),
(261, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:09', '2021-03-17 14:08:09'),
(262, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:09', '2021-03-17 14:08:09'),
(263, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:10', '2021-03-17 14:08:10'),
(264, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:11', '2021-03-17 14:08:11'),
(265, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:14', '2021-03-17 14:08:14'),
(266, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:15', '2021-03-17 14:08:15'),
(267, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:31', '2021-03-17 14:08:31'),
(268, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:31', '2021-03-17 14:08:31'),
(269, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:32', '2021-03-17 14:08:32'),
(270, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:40', '2021-03-17 14:08:40'),
(271, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:55', '2021-03-17 14:08:55'),
(272, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:56', '2021-03-17 14:08:56'),
(273, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.com', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-03-17 14:08:58', '2021-03-17 14:08:58'),
(274, 'yes', 'no', 'Christopher', 'Christopher.schoettle@towerhealth.org', 'Pharm', 'Reading Hospital', 'Reading', 'PA', 'Peg Ridolfi', 'Health System', '2021-03-17 14:10:38', '2021-03-17 14:10:38'),
(275, 'yes', 'no', 'Kim', 'Kimberly.carobine@towerhealth.org', 'RN', 'Reading Hospital', 'Reading', 'PA', 'Peg Ridolfi', 'Health System', '2021-03-17 17:22:18', '2021-03-17 17:22:18'),
(276, 'yes', 'no', 'Nicole Noveck', 'Nicole.noveck@gmail.com', 'Transplant Coordinator', 'Montefiore Medical Center', 'Bronx', 'New York', 'Arlene Tejada', 'Health System', '2021-03-19 15:01:55', '2021-03-19 15:01:55'),
(277, 'yes', 'no', 'STEPHANIE LATOUR', 'STEPHANIE.LATOUR@OCHSNER.ORG', 'PATIENT CARE ADVOCATE', 'OCHSNER', 'BATON ROUGE', 'LA', 'CATHERINE LEACH', 'Health System', '2021-03-22 17:27:41', '2021-03-22 17:27:41'),
(278, 'yes', 'no', 'Tausha Pearson', 'Tausha.pearson@ochsner.org', 'PharmD', 'OCHSNER Pharmacy and Wellness', 'Baton Rouge', 'Louisiana', 'Catherine Leach', 'Health System', '2021-03-22 17:30:38', '2021-03-22 17:30:38'),
(279, 'yes', 'no', 'Deatrice Givens', 'deatrice.givens@ochsner.org', 'Cpht', 'OCHSNER Clinic Foundation', 'Baton Rouge', 'LA', 'Catherine Leach', 'Health System', '2021-03-22 17:37:24', '2021-03-22 17:37:24'),
(280, 'yes', 'no', 'Teresa Garber', 'teresa.garber@franciscanalliance.org', 'PharmD', 'Franciscan Health Indianapolis', 'Indianapolis', 'IN', 'Melissa Barger', 'Health System', '2021-03-22 23:21:07', '2021-03-22 23:21:07'),
(281, 'yes', 'yes', 'LeAnn Doddridge', 'LeAnn.Doddridge@BHSI.com', 'Pharm D, Clinical Supervisor', 'Baptist Health Floyd', 'New Albany', 'IN', 'Melissa Barger', 'Health System', '2021-03-24 13:27:32', '2021-03-24 13:27:32'),
(282, 'yes', 'no', 'Autumn Helm', 'autumn.helm@ascension.org', 'Pharm D', 'Ascension St. Vincent - Indianapolis', 'Indianapolis', 'IN', 'Melissa Barger', 'Large Group Practice or Physician Group', '2021-03-29 20:33:14', '2021-03-29 20:33:14'),
(283, 'yes', 'yes', 'Tracey Whipple', 'tracey.whipple@unitypoint.org', 'LPN', 'Unity Point Health Care', 'Does Moines', 'Iowa', 'Laura Skarperud', 'Large Group Practice or Physician Group', '2021-03-31 17:35:28', '2021-03-31 17:35:28'),
(284, 'yes', 'yes', 'test test', 'test@test.com', 'test', 'test', 'test', 'Clare', 'test', 'Health System', '2021-04-02 12:30:42', '2021-04-02 12:30:42'),
(285, 'yes', 'yes', 'test test', 'test@test.com', 'test', 'test', 'test', 'Connecticut', NULL, 'Large Group Practice or Physician Group', '2021-04-02 12:37:25', '2021-04-02 12:37:25'),
(286, 'yes', 'yes', 'test testtest', 'test@test.com', 'test', 'test', 'test', 'Connecticut', NULL, 'Health System', '2021-04-02 12:40:44', '2021-04-02 12:40:44'),
(287, 'yes', 'no', 'test test', 'test@test.com', 'test', 'test', 'test', 'Clare', NULL, 'Large Group Practice or Physician Group', '2021-04-02 12:42:28', '2021-04-02 12:42:28'),
(288, 'no', 'yes', 'test test', 'test@test.com', 'test', 'test', 'test', 'test', NULL, 'Health System', '2021-04-02 12:47:51', '2021-04-02 12:47:51'),
(289, 'yes', 'no', 'test test', 'test@test.com', 'test', 'test', 'test', 'Clare', NULL, 'Health System', '2021-04-02 12:49:13', '2021-04-02 12:49:13'),
(290, 'yes', 'no', 'test test', 'test@test.com', 'test', 'test', 'test', 'Clare', NULL, 'Health System', '2021-04-02 13:15:54', '2021-04-02 13:15:54'),
(291, 'yes', 'yes', 'test test', 'test@test.com', 'test', 'test', 'test', 'Clare', NULL, 'Health System', '2021-04-02 13:19:35', '2021-04-02 13:19:35'),
(292, 'yes', 'no', 'Anna Laviolette', 'anna.laviolette@walgreens.com', 'Pharmacy Manager', 'Walgreens Local Specialty', 'Marrero', 'Louisiana', 'Catherine Leach', NULL, '2021-04-02 17:41:46', '2021-04-02 17:41:46'),
(293, 'no', 'yes', 'Marissa Guillen', 'maguillen@mercy.com', 'Pharm D', 'Mercy Fairfield', 'Fairfield', 'Ohio', 'Melissa Barger', 'Health System', '2021-04-06 16:02:08', '2021-04-06 16:02:08'),
(294, 'no', 'yes', 'Marissa Guillen', 'maguillen@mercy.com', 'Pharm D', 'Mercy Fairfield', 'Fairfield', 'Ohio', 'Melissa Barger', 'Health System', '2021-04-06 16:02:09', '2021-04-06 16:02:09'),
(295, 'yes', 'no', 'Valarie Francis', 'valarie.francis@ochsnerlsuhs.org', 'Case Worker', 'Ochsner LSU Health', 'Shreveport', 'LA', 'Catherine Leach', 'Large Group Practice or Physician Group', '2021-04-06 20:11:22', '2021-04-06 20:11:22'),
(296, 'yes', 'no', 'Stacie Johnson', 'stacie.johnson@pennmedicine.upenn.edu', 'Pharmacist', 'Lancaster General Hospital', 'Lancaster', 'Pa', 'Peg Ridolfi', 'Health System', '2021-04-08 14:31:11', '2021-04-08 14:31:11'),
(297, 'yes', 'no', 'Nancy McKinstry', 'nmck0001@shands.ufl.edu', 'Case Manager', 'UF Shands', 'Gainesville', 'Florida', NULL, 'Health System', '2021-04-08 15:47:04', '2021-04-08 15:47:04'),
(298, 'yes', 'no', 'Nancy McKinstry', 'nmck0001@shands.ufl.edu', 'Case Manager', 'UF Shands', 'Gainesville', 'Florida', NULL, 'Health System', '2021-04-08 15:47:04', '2021-04-08 15:47:04'),
(299, 'yes', 'no', 'Nancy McKinstry', 'nmck0001@shands.ufl.edu', 'Case Manager', 'UF Shands', 'Gainesville', 'Florida', NULL, 'Health System', '2021-04-08 15:47:04', '2021-04-08 15:47:04'),
(300, 'yes', 'no', 'Nancy McKinstry', 'nmck0001@shands.ufl.edu', 'Case Manager', 'UF Shands', 'Gainesville', 'Florida', NULL, 'Health System', '2021-04-08 15:47:05', '2021-04-08 15:47:05'),
(301, 'yes', 'no', 'Nancy McKinstry', 'nmck0001@shands.ufl.edu', 'Case Manager', 'UF Shands', 'Gainesville', 'Florida', NULL, 'Health System', '2021-04-08 15:47:05', '2021-04-08 15:47:05'),
(302, 'yes', 'no', 'Wendy Barry', 'wendy.barry@franklynhc.com', 'Account', 'Aventria', 'Doylestown', 'PA', 'NA', 'Health System', '2021-04-08 16:29:25', '2021-04-08 16:29:25'),
(303, 'yes', 'no', 'Shanna Whitwell', 'Shanna.whitwell@bmhcc.org', 'Pharmacy Manager', 'Baptist Family Pharmacy', 'Jackson', 'MS', 'Catherine Leach', 'Health System', '2021-04-08 17:19:27', '2021-04-08 17:19:27'),
(304, 'yes', 'no', 'Test-KK', 'kathleen.kurtz@franklynhc.com', 'TEST', 'Aventria', 'Parsippany', 'NJ', NULL, 'Health System', '2021-04-08 17:48:45', '2021-04-08 17:48:45'),
(305, 'yes', 'no', 'Tasha Botkin', 'Natasha.Botkin@unitypoint.org', 'RN', 'Unity Point Gastroenterology', 'Cedar Ra', 'Iowa', 'Derrick', 'Large Group Practice or Physician Group', '2021-04-08 18:18:18', '2021-04-08 18:18:18'),
(306, 'yes', 'yes', 'Sandra Arts', 'sandra.arts@aventriahealth.com', 'Title Here', 'SDA, Inc.', 'Doylestown', 'Pennsylvania', NULL, NULL, '2021-04-08 18:48:28', '2021-04-08 18:48:28'),
(307, 'yes', 'yes', 'Katy Williams, MBA, RN', 'katheryn.williams@mercy.net', 'GI Supervisor over all GI clinics with Mercy', 'Mercy Health System', 'St. Louis', 'MO', 'Laura Skarperud', 'Large Group Practice or Physician Group', '2021-04-09 15:37:22', '2021-04-09 15:37:22'),
(308, 'yes', 'yes', 'Daniel Poor', 'daniel_poor@teamhealth.com', 'ER Doc.', 'Baptist ER', 'Memphis', 'TN', 'John Dees', 'Health System', '2021-04-12 15:40:34', '2021-04-12 15:40:34'),
(309, 'yes', 'yes', 'Prashanth raj', 'testing@testing.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Health System', '2021-04-14 04:52:25', '2021-04-14 04:52:25'),
(310, 'yes', 'no', 'Sabrina Brown', 'sbrown11@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:01:19', '2021-04-14 19:01:19'),
(311, 'yes', 'no', 'Sherry Jones', 'sdjones2@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:02:24', '2021-04-14 19:02:24'),
(312, 'yes', 'no', 'Andrea Hubbard', 'arhubbard@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:03:16', '2021-04-14 19:03:16'),
(313, 'yes', 'no', 'Angela Macklin', 'amacklin@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:04:11', '2021-04-14 19:04:11'),
(314, 'yes', 'no', 'Angela Davis', 'addavis2@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:05:02', '2021-04-14 19:05:02'),
(315, 'yes', 'no', 'Jeannine Griffith', 'jgriffith3@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:05:58', '2021-04-14 19:05:58'),
(316, 'yes', 'no', 'Kimberly Qualls', 'kqualls@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:06:46', '2021-04-14 19:06:46'),
(317, 'yes', 'no', 'Sonja Bullock', 'sbullock@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:07:35', '2021-04-14 19:07:35'),
(318, 'yes', 'no', 'Angela Booze', 'abooze@umc.edu', 'Case Management', 'University of Mississippi Medical Center', 'Jackson', 'Mississippi', 'Catherine Leach', 'Health System', '2021-04-14 19:08:17', '2021-04-14 19:08:17'),
(319, 'yes', 'no', 'test test', 'test@test.com', 'test', 'test', 'test', 'Clare', NULL, 'Large Group Practice or Physician Group', '2021-04-15 14:31:11', '2021-04-15 14:31:11'),
(320, 'yes', 'yes', 'Rebecca Vanderveer', 'rebecca.vanderveer@mclaren.org', 'medical  assistant', 'Family First Macomb', 'Clinton Twsp', 'Michigan', 'Dianna Higgins', 'Health System', '2021-04-15 16:30:47', '2021-04-15 16:30:47'),
(321, 'yes', 'no', 'Jumie Jacob', 'Jumie.jacob@towerhealth.org', 'Pharmacist', 'Reading Hospital', 'Reading', 'Pa', 'Peg Ridolfi', 'Health System', '2021-04-15 18:16:28', '2021-04-15 18:16:28'),
(322, 'yes', 'no', 'Rachel Smith', 'Rachel.smith2@towerhealth.org', 'Pharmacist', 'Reading Hospital', 'Reading', 'PA', 'Peg Ridolfi', 'Health System', '2021-04-15 18:31:43', '2021-04-15 18:31:43'),
(323, 'yes', 'yes', 'Vishal Patel', 'Vishalpatelmd@gmail.com', 'MD', 'Reading Hospital', 'Reading', 'PA', 'Peg Ridolfi', 'Health System', '2021-04-15 18:40:17', '2021-04-15 18:40:17'),
(324, 'yes', 'no', 'Estrella Thendrex', 'thendrex.estrella@halifax.org', 'MD', 'Halifax Health', 'Daytona', 'Florida', 'Madeline Pruitt', 'Health System', '2021-04-19 20:26:21', '2021-04-19 20:26:21'),
(325, 'yes', 'no', 'Estrella Thendrex', 'thendrex.estrella@halifax.org', 'MD', 'Halifax Health', 'Daytona', 'Florida', 'Madeline Pruitt', 'Health System', '2021-04-19 20:26:22', '2021-04-19 20:26:22'),
(326, 'yes', 'no', 'Estrella Thendrex', 'thendrex.estrella@halifax.org', 'MD', 'Halifax Health', 'Daytona', 'Florida', 'Madeline Pruitt', 'Health System', '2021-04-19 20:26:22', '2021-04-19 20:26:22'),
(327, 'yes', 'no', 'Estrella Thendrex', 'thendrex.estrella@halifax.org', 'MD', 'Halifax Health', 'Daytona', 'Florida', 'Madeline Pruitt', 'Health System', '2021-04-19 20:26:22', '2021-04-19 20:26:22'),
(328, 'yes', 'no', 'Tami Kipple', 'kipple.tami@mayo.edu', 'ARNP', 'Mayo Clinic', 'Jacksonville', 'FL', 'Madeline Pruitt', 'Health System', '2021-04-19 20:39:10', '2021-04-19 20:39:10'),
(329, 'yes', 'no', 'Tami Kipple', 'kipple.tami@mayo.edu', 'ARNP', 'Mayo Clinic', 'Jacksonville', 'FL', 'Madeline Pruitt', 'Health System', '2021-04-19 20:39:11', '2021-04-19 20:39:11'),
(330, 'yes', 'yes', 'Ami Naik', 'top_med@yahoo.com', 'Office Manager', 'Henderson County Medical Group', 'Henderson', 'TN', 'John Dees', 'Large Group Practice or Physician Group', '2021-04-20 15:28:43', '2021-04-20 15:28:43'),
(331, 'yes', 'yes', 'Joe Garrido', 'umichgrad08@gmail.com', 'Medical Doctor', 'HCA Centennial', 'Nashville', 'TN', 'John Dees', 'Health System', '2021-04-20 15:34:03', '2021-04-20 15:34:03'),
(332, 'no', 'yes', 'Katy Brogan', 'katy.brogan@uoflhealth.org', 'Pharm D - DOP', 'University of Louisville', 'Louisville', 'KY', 'Melissa Barger', 'Health System', '2021-04-20 18:31:02', '2021-04-20 18:31:02'),
(333, 'yes', 'yes', 'Michelle Bartoo', 'michelle.bartoo@lvhn.org', 'Hospitalist Administrator', 'Lehigh Valley Muhlenberg', 'Bethlehem', 'Pa', 'Peg Ridolfi', 'Health System', '2021-04-21 17:21:54', '2021-04-21 17:21:54'),
(334, 'yes', 'yes', 'Gina Guzman', 'gguzman@umcelpaso.org', 'Director', 'University Medical Center El Paso', 'El Paso', 'Texas', 'Kevin Akin', 'Health System', '2021-04-22 14:41:45', '2021-04-22 14:41:45'),
(335, 'yes', 'yes', 'Marintha Short', 'marintha.short@commonspirit.org', 'Pharm D', 'St. Joseph Hospital -- Continuing Care Hospital', 'Lexington', 'KY', 'Melissa Barger', 'Health System', '2021-04-23 03:31:54', '2021-04-23 03:31:54'),
(336, 'no', 'yes', 'testing', 'testing@testing.com', 'partner', 'testing', 'Jersey City', 'NJ', 'Prashanth Pandula', 'Health System', '2021-04-26 12:42:40', '2021-04-26 12:42:40'),
(337, 'no', 'yes', 'Heather', 'heather.hellebuyck@salix.com', 'Territory sales manager', 'Salix', 'Chesterfield', 'Mi', 'Heather hellebuyck', NULL, '2021-04-27 14:59:25', '2021-04-27 14:59:25'),
(338, 'yes', 'yes', 'Dawn Hermann', 'dawn.hermann@mclaren.org', 'medical  assistant', 'McLaren Macomb', 'Clinton', 'Michigan', 'Dianna Higgins', 'Health System', '2021-04-27 16:40:53', '2021-04-27 16:40:53'),
(339, 'yes', 'yes', 'Rachel Tokarski', 'Rachel.tokarski@mclaren.org', 'medical  assistant', 'McLaren Macomb', 'Clinton', 'Michigan', 'Dianna Higgins', 'Health System', '2021-04-27 16:42:17', '2021-04-27 16:42:17'),
(340, 'yes', 'yes', 'Jocelyn Kipp', 'jocelyn.kipp@mclaren.org', 'medical  assistant', 'McLaren Macomb', 'clinton', 'Michigan', 'Dianna Higgins', 'Health System', '2021-04-27 16:44:03', '2021-04-27 16:44:03'),
(341, 'yes', 'yes', 'Amy Perpich', 'amy.perpich@nortonhealthcare.org', 'Pharm D', 'Norton Healthcare', 'Louisville', 'KY', 'Melissa Barger', 'Health System', '2021-04-27 20:52:57', '2021-04-27 20:52:57'),
(342, 'no', 'yes', 'mark', 'brow@gmail.com', 'markbrown', 'markbrown', 'NY', 'NY', 'n/a', NULL, '2021-04-28 14:08:29', '2021-04-28 14:08:29'),
(343, 'yes', 'yes', 'Rick Mann', 'rmann@llu.edu', 'Pharmacist', 'Loma Linda Medical Center', 'Loma Linda', 'CA', 'Rebecca OMarrah', 'Health System', '2021-04-28 18:12:21', '2021-04-28 18:12:21'),
(344, 'yes', 'no', 'Erica Jones', 'ejones2@uams.edu', 'Nursing', 'University of Arkansas Gastroenterology', 'Little Rock', 'Arkansas', 'Catherine Leach', 'Large Group Practice or Physician Group', '2021-04-29 17:40:02', '2021-04-29 17:40:02'),
(345, 'yes', 'no', 'Molly Murtaugh', 'molly.murtaugh@bhsi.com', 'Pharm D', 'Baptist Health Floyd', 'New Albany', 'IN', 'Melissa Barger', 'Health System', '2021-04-30 16:58:27', '2021-04-30 16:58:27'),
(346, 'no', 'yes', 'heather hellebuyck', 'heather.hellebuyck@salix.com', 'territory sales manager', 'salix', 'chesterfield', 'michigan', 'diana higgins', NULL, '2021-05-03 15:54:32', '2021-05-03 15:54:32'),
(347, 'yes', 'no', 'Leah Brady', 'leah.brady@lpnt.net', 'Case Manager', 'Georgetown Community Hospital', 'Georgetown', 'KY', 'Melissa Barger', 'Health System', '2021-05-05 21:45:44', '2021-05-05 21:45:44'),
(348, 'yes', 'no', 'Senayish Addis', 'senayish.addis@pennmedicine.upenn.edu', 'SW', 'HUP', 'Philadelphia', 'PA', 'Peg Ridolfi', 'Health System', '2021-05-06 16:25:22', '2021-05-06 16:25:22'),
(349, 'yes', 'no', 'Carlos Manalich', 'carlos.manalich@gmail.com', 'MD', 'Self Regional Health Care', 'Greenwood', 'SC', NULL, 'Health System', '2021-05-06 19:12:25', '2021-05-06 19:12:25'),
(350, 'yes', 'yes', 'Deborah Goelzhauser', 'Deborah.Goelzhauser@christushealth.org', 'RN Case Manager', 'Christus Santa Rosa Hospital - Westover Hills', 'San Antonio', 'TX', 'Mike Bragg', 'Health System', '2021-05-10 19:30:15', '2021-05-10 19:30:15'),
(351, 'yes', 'no', 'asdlak', 'alsdka@gmail.com', 'alsdkas', 'laskdas', 'lasdkal', 'asldk', 'asdlk', 'Large Group Practice or Physician Group', '2021-05-11 20:38:04', '2021-05-11 20:38:04'),
(352, 'yes', 'no', 'Monica De Robertis Costa', 'mderobertiscosta@tgh.org', 'Specialty Pharmacy Manager', 'Tampa General', 'Tampa', 'FL', 'Madeline Pruitt', 'Health System', '2021-05-13 15:38:24', '2021-05-13 15:38:24'),
(353, 'yes', 'no', 'mark', 'makr@gmail.com', 'markbrown', 'Rich Brownburg', 'test', 'New York', 'MArk', 'Large Group Practice or Physician Group', '2021-05-17 18:09:29', '2021-05-17 18:09:29'),
(354, 'yes', 'no', 'mark', 'makr@gmail.com', 'markbrown', 'Rich Brownburg', 'test', 'New York', 'MArk', 'Large Group Practice or Physician Group', '2021-05-17 18:09:29', '2021-05-17 18:09:29'),
(355, 'yes', 'no', 'Dr. Brandon McGuire', 'smcfall@uabmc.edu', 'Medical Doctor', 'UAB', 'Birmingham', 'Alabama', 'John Dees', 'Health System', '2021-05-17 19:20:34', '2021-05-17 19:20:34'),
(356, 'no', 'yes', 'akak', 'akaak@gmail.com', 'alsak', 'alskal', 'alskda', 'alsdk', 'asldk', 'Health System', '2021-05-20 15:47:14', '2021-05-20 15:47:14'),
(357, 'yes', 'yes', 'Sanita Piya', 'spiya@fallhillgastro.com', 'Np', 'FHGA', 'Fredericksburg', 'VA', 'Dan Hawkins', 'Large Group Practice or Physician Group', '2021-05-20 16:17:00', '2021-05-20 16:17:00'),
(358, 'yes', 'yes', 'Kathy Stevens', 'kstevens@fallhillgastro.com', 'Nurse', 'Fall Hill Gastro', 'Fredericksburg', 'VA', 'Daniel Hawkins', 'Large Group Practice or Physician Group', '2021-05-20 16:42:51', '2021-05-20 16:42:51'),
(359, 'no', 'yes', 'test test', 'test@test.com', 'test', 'test', 'test', 'Connecticut', NULL, 'Health System', '2021-05-21 12:13:01', '2021-05-21 12:13:01'),
(360, 'no', 'no', 'test test', 'test@test.com', 'test', 'test', 'test', 'Clare', NULL, 'Large Group Practice or Physician Group', '2021-05-21 12:16:40', '2021-05-21 12:16:40'),
(361, 'yes', 'yes', 'kayla', 'kayla_toothman@chs.net', 'CMA', 'Plateau Clinic', 'oak hill', 'WV', 'Deborah Norris', 'Large Group Practice or Physician Group', '2021-05-26 16:15:17', '2021-05-26 16:15:17'),
(362, 'yes', 'no', 'Kristin Guerin', 'kristin.guerin@brgeneral.com', 'Care Coordinator', 'Baton Rouge General', 'Baton Rouge', 'LA', 'Catherine Leach', 'Health System', '2021-05-27 16:24:07', '2021-05-27 16:24:07'),
(363, 'yes', 'no', 'Kristin Guerin', 'kristin.guerin@brgeneral.com', 'Care Coordinator', 'Baton Rouge General', 'Baton Rouge', 'Louisiana', 'Catherine Leach', 'Health System', '2021-05-27 16:24:27', '2021-05-27 16:24:27'),
(364, 'yes', 'no', 'Kristin Guerin', 'kristin.guerin@brgeneral.org', 'Care Coordinator', 'Baton Rouge General', 'Baton Rouge', 'Louisiana', 'Catherine Leach', 'Health System', '2021-05-27 16:25:07', '2021-05-27 16:25:07'),
(365, 'yes', 'no', 'Kristin Guerin', 'kristin.guerin@brgeneral.org', 'Care Coordinator', 'Baton Rouge General', 'Baton Rouge', 'Louisiana', 'Catherine Leach', 'Health System', '2021-05-27 16:25:07', '2021-05-27 16:25:07'),
(366, 'yes', 'no', 'Leslee Taylor', 'leslee.taylor@brgeneral.org', 'Case Manager', 'Baton Rouge General', 'Baton Rouge', 'Louisiana', 'Catherine Leach', 'Health System', '2021-05-27 16:26:08', '2021-05-27 16:26:08'),
(367, 'yes', 'yes', 'Jinhee Sung', 'jinhee.sung@beaumont.org', 'Pharmacist', 'Beaumont Farmington Hills', 'Farmington Hills', 'Michigan', 'Dianna Higgins', 'Health System', '2021-06-01 16:26:56', '2021-06-01 16:26:56'),
(368, 'yes', 'no', 'Jennifer Johannesson', 'jennifer.johannesson@pennmedicine.upenn.edu', 'Transplant Social Worker', 'Hospital of the University of Pennsylvania', 'Philadelphia', 'Pa', 'Peg Ridolfi', 'Health System', '2021-06-02 15:35:31', '2021-06-02 15:35:31'),
(369, 'yes', 'yes', 'Carolyn Driscoll', 'cdriscoll@cvillegi.com', 'NP', 'Charlottesville', 'Charlottesville', 'VIRGINIA', 'Dan Hawkins', 'Large Group Practice or Physician Group', '2021-06-02 15:38:03', '2021-06-02 15:38:03'),
(370, 'yes', 'yes', 'Carolyn Driscoll', 'cdriscoll@cvillegi.com', 'NP', 'Charlottesville', 'Charlottesville', 'VIRGINIA', 'Dan Hawkins', 'Large Group Practice or Physician Group', '2021-06-02 15:38:04', '2021-06-02 15:38:04'),
(371, 'yes', 'yes', 'Libbie Bandy', 'libbiebandy@gmail.com', 'Nurse', 'University of Kansas Medical Center', 'Kansas City, KS', 'Kansas', 'Laura Skarperud', 'Health System', '2021-06-03 18:12:35', '2021-06-03 18:12:35'),
(372, 'yes', 'yes', 'Erica Torres', 'erica.torres@dignityhealth.org', 'Pharmacy Buyer', 'Chandler Regional', 'Chandler', 'Arizona', 'Kevin Akin', 'Health System', '2021-06-03 22:32:42', '2021-06-03 22:32:42'),
(373, 'yes', 'yes', 'Ivan Lau', 'ivan.lau@bannerhealth.com', 'Phamacy Manager', 'Banner Thunderbird', 'Phoenix', 'Arizona', 'Kevin Akin', 'Health System', '2021-06-03 22:42:35', '2021-06-03 22:42:35'),
(374, 'yes', 'yes', 'Caryn Fisher', 'caryn.fisher@valleywisehealth.org', 'Pharmacist', 'Valleywise Health', 'Phoenix', 'Arizona', 'Kevin Akin', 'Health System', '2021-06-04 15:28:07', '2021-06-04 15:28:07'),
(375, 'yes', 'yes', 'Abhijeet jatania', 'aj@bergenrx.com', 'Pharmacist in charge', 'Bergen pharmacy', 'Newark', 'New jersey', 'Alejandro Robayo', NULL, '2021-06-07 20:46:27', '2021-06-07 20:46:27'),
(376, 'yes', 'yes', 'Whittney Maupin', 'wmaupin@cvillegi.com', 'RN', 'Charlottesville Gastroenterology Associates', 'Charlottesville', 'Virginia', NULL, 'Large Group Practice or Physician Group', '2021-06-11 15:33:15', '2021-06-11 15:33:15'),
(377, 'yes', 'yes', 'Sara English', 'sara.english@ascension.org', 'Case Manager', 'Ascension Southfield Providence', 'Southfield', 'Michigan', 'Dianna Higgins', 'Health System', '2021-06-11 17:36:07', '2021-06-11 17:36:07'),
(378, 'yes', 'yes', 'Jerri Karaffa', 'Jerri.karaffa@ascension.org', 'Case Manager', 'Ascension Providence Southfield', 'Southfield', 'Michigan', 'Dianna Higgins', 'Health System', '2021-06-11 17:37:19', '2021-06-11 17:37:19'),
(379, 'yes', 'yes', 'Bette Kelly', 'bette.kelly@ascension.org', 'Social Worker', 'Ascension Southfield Providence', 'Southfield', 'Michigan', 'Dianna Higgins', 'Health System', '2021-06-11 17:38:25', '2021-06-11 17:38:25'),
(380, 'no', 'yes', 'test', 'carolyn.odenwald@aventriahealth.com', 'test', 'PINNACLE HEALTH COMMUNICATIONS', 'Doylestown', 'PA', NULL, 'Agency', '2021-06-11 22:31:18', '2021-06-11 22:31:18'),
(381, 'yes', 'yes', 'Jennifer Burns', 'jennifer.burns@ascension.org', 'Social Worker', 'Ascension PROVIDENCE SOUTHFIELD', 'SOUTHFIELD', 'Michigan', 'Dianna Higgins', 'Health System', '2021-06-12 15:53:45', '2021-06-12 15:53:45'),
(382, 'yes', 'yes', 'Beata Jedrzejczyk', 'beata.jedrzejczyk@ascension.org', 'Case manager', 'Southfield providence', 'Southfield', 'Michigan', 'Dianna Higgins', 'Health System', '2021-06-12 16:15:43', '2021-06-12 16:15:43'),
(383, 'yes', 'no', 'Katie Watson', 'kwatson@harringtonhospital.org', 'Hospitalist Coordinator', 'Harrington Hospital', 'Southbridge', 'MA', 'Erin Newman', 'Health System', '2021-06-14 16:49:21', '2021-06-14 16:49:21'),
(384, 'yes', 'yes', 'Donald Hillebrand, MD', 'dhillebrand@saint-lukes.org', 'Physician', 'Saint Luke’s Health System', 'Kansas City', 'Missouri', 'Laura Skarperud', 'Health System', '2021-06-15 15:06:07', '2021-06-15 15:06:07'),
(385, 'yes', 'no', 'Lucy Holleman', 'lholleman@mhg.com', 'Pharmacist', 'Memorial at Gulfport', 'Gulfport', 'Ms', 'Catherine Leach', 'Health System', '2021-06-16 19:33:43', '2021-06-16 19:33:43');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(2, 'user', 'Normal User', '2020-04-02 00:00:01', '2020-04-02 00:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\April2020\\82kHVnwCN6CGWnMTteRI.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Xifaxan Admin', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Xifaxan Admin Panel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Xifaxan Admin', 'admin@admin.com', 'users\\April2020\\431MwDY8BMPPsTowxj55.png', NULL, '$2y$10$nu6xBfpoRAWr3BnBKcNhc.3e.T2i6wPXrj2uuIpzgRYBs1bXm6cnK', NULL, '{\"locale\":\"en\"}', '2020-04-02 00:04:45', '2020-12-29 15:19:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ctas`
--
ALTER TABLE `ctas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_pages`
--
ALTER TABLE `home_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plugin_orders`
--
ALTER TABLE `plugin_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ctas`
--
ALTER TABLE `ctas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_pages`
--
ALTER TABLE `home_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `plugin_orders`
--
ALTER TABLE `plugin_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=386;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'PagesController@index')->name('home');
Route::get('/sample-plugin', 'PagesController@samplePlugin')->name('samplePlugin');
Route::post('/submit-form','PagesController@pluginFormSubmit')->name('plugin.form.submit');
Route::get('/overviewflashcard','PagesController@viewPdf')->name('flashcard');
Route::get('/prescribing-information','PagesController@pi')->name('pi');
Route::get('/ordersetkit', 'PagesController@getDownload');

Route::group(['prefix' => 'api'], function()
{
	Route::get('/get-order-details','PagesController@getOrder');
	Route::get('/get-user','PagesController@getUser');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

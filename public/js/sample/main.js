$(document).ready(function() {

    stickyHeader();
    $(window).scroll(function() {
        stickyHeader();
    });


 /* ACCORDIAN */
    $('.acc__title .drop-down ').click(function(j) {

        var dropDown = $(this).closest('.acc__card').find('.acc__panel');
        $(this).closest('.acc').find('.acc__panel').not(dropDown).slideUp();

        if ($(this).parent('.acc__title').hasClass('active')) {
            $(this).parent('.acc__title').removeClass('active');
        } else {
            $(this).closest('.acc').find('.acc__title.active').removeClass('active');
            $(this).parent('.acc__title').addClass('active');
        }

        dropDown.stop(false, true).slideToggle();
        j.preventDefault();
    });


 /* BACK TO TOP */
    var btn = $('.back-to-top');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });


    $('.pglink a').click(function (e) {
        e.preventDefault();
        var target = $($(this).attr('href'));
        if (target.length) {
            var scrollTo = target.offset().top;
            $('body, html').animate({ scrollTop: scrollTo + 'px' }, 800);
        }
    });





    /* tabs */
    $('ul.tabs li').click(function() {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('active');
      $('.tab-content').removeClass('current');

      $(this).addClass('active');
        $("#" + tab_id).addClass('current');
    })



    /* Menu */
    $(document).on('click', '.menu-icon', function () {
        $('.mob-menu-up').fadeIn();
    })
    $(document).on('click', '.mob-close', function () {
        $('.mob-menu-up').fadeOut();
    })
    /* //Menu */


    

    $('#copy-tool').on('click', function (e) {
        

        var value = [];
        var value = $('input[type=checkbox]:checked').map(function (_, el) {
            return (' ' + $(el).data('tool-url'));
        }).get();
        
        if ($("input[type=checkbox]").is(":checked")) {
            // console.log("Copied the text: " + value); 
            // var $temp = $("<input type='text'>");
            // alert($temp);
            // $('body').append($temp);
            // $temp.val(value).select();
            // document.execCommand("copy");

            // $('.copy-cbd').fadeIn();
            // setTimeout(function () { $('.copy-cbd').fadeOut(); }, 3000);
            
        }
        else if ($("input[type=checkbox]").is(":not(:checked)")) {
            $('.uncopy-cbd').fadeIn();
            setTimeout(function () { $('.uncopy-cbd').fadeOut(); }, 3000);
        }
        
        e.preventDefault();
        //alert(sel);      
         
    }); 



   
    

});




/* 
function () {
    /* Get the text field  
    var copyText = document.getElementById("myInput");

    /* Select the text field  
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices 

    /* Copy the text inside the text field  
    document.execCommand("copy");

    /* Alert the copied text  
    alert("Copied the text: " + copyText.value);
} */





function stickyHeader() {
    var sticky = $('header'),
        scroll = $(window).scrollTop();

    if (scroll >= 40) sticky.addClass('fixHeader');
    else sticky.removeClass('fixHeader');
}
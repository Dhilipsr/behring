<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> HIZENTRA – QSA – SAMPLE PLUGIN</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="Hizentra_favicon-01-01.jpg">
    <link rel="icon" href="{{asset('Hizentra_favicon-01-01.jpg')}}">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('sample/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('sample/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/sampleMain.css')}}">

</head>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
		<![endif]-->

    <!-- Header Starts -->

    <div class="frame ">
        <div class="samp-img"><i>sample</i></div>
        {{-- <div class="manu-sec">
            <div class="mob-menu-box">

                <div class="menu-icon"><span></span>
                    <p>menu</p>
                </div>
                <div class="mamber-logo"><img src="{{asset('img/man-logo.png')}}"></div>
            </div>
        </div>
        <div class="mob-menu-up">
            <div class="mob-menu">
                <div class="mob-close-box"><span class="mob-close"><img src="img/close-white.png" alt="img"></span>
                </div>
                <ul class="navigation">
                    <li><a href="login.html">Log In</a></li>

                </ul>
            </div>
        </div> --}}
        <header class="">
            <div class="side-space">
                <section class="container">
                    <div class="row">
                        <div class="col-12  ">
                            <div class="bgContainer">
                                <div class="logo" style="padding"><img src="{{asset('sample/img/logo1.png')}}">
                                </div>
                                <!--  <div class="support"><img src="img/qsa.png"></div> -->
                                <div class="indication">
                                    <h6>Indications
                                    </h6> Hizentra<sup>®</sup>, Immune Globulin Subcutaneous (Human), 20% Liquid, is indicated for:<br>

<div class="tea"><ul><li>Treatment of primary  immunodeficiency (PI) in adults and pediatric patients 
 2 years and older.</li></ul></div>

<div class="tea1"><ul><li>Maintenance therapy in adults with chronic inflammatory demyelinating 
 polyneuropathy (CIDP) to prevent relapse of neuromuscular disability and
 impairment.</li></ul></div>
  <div class="lim" style="padding-left: 37px;
    text-indent: -15px;">
   – Limitation of use: maintenance therapy in CIDP has been 
   systematically 
 studied for 6 months and for a further 12 months in a follow-up study. 
 Continued maintenance beyond these periods should be individualized 
 based on patient response and need for continued therapy.</div>
<span class="for">For subcutaneous infusion only.</span>


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </header>
       
        <!-- Header Ends -->



        <!-- Main Section -->
        

       <form class="form">
            <section class="side-space main-section">
                <section class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="top-link ">
                                <ul>
                                    <li class="pglink"><a href="#provider">Provider</a></li>
                                    <li class="pglink"><a href="#patients"> Patient</a></li>
                                  <!--   {{ route('pi') }} -->
                                    <li><a href="https://labeling.cslbehring.com/PI/US/Hizentra/EN/Hizentra-Prescribing-Information.pdf"
                                            target="_blank">Prescribing Information</a></li>
                                    <li><a href="#isi" data-scroll> Important Safety Information</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-12">
                            <nav class="notification">
                                <ul>
                                    <li><img src="{{asset('sample/img/select.png')}}"> Select tool(s)</li>
                                    <li><img src="{{asset('sample/img/search.png')}}"> Preview tool</li>
                                    <li><img src="{{asset('img/arrow.png')}}"> Description</li>

                                </ul>
                            </nav>
                        </div>
                    </div>

                    <div class="row headings " id="provider">
                        <div class="col-6">
                            <div class="mr-left fsdf">
                                <span >
                                <h3 class="pro">PROVIDER</h3>
                            </span>
                            </div>
                        </div>
                        <!--  <div class="col-6">
                            <small>*These resources cannot be printed.</small>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-12  mr-left">
                           <!-- this is commentd by me -->
                           <!--  <div class="pd-left sub-title">
                                <h3>Resources</h3>
                            </div> -->
                        </div>
                    </div>


                    
                        
                        
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="01"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Considerations for PAs">
                                            
                                              <label class="dfdd" for="01"></label> <div class="adn" >Optimizing the Transition to Hizentra for CIDP Patients</div></label>
                                                     
                                            </span>
                                            <a href="https://cc-oge.online/hiz-cidp-cslb-sample/optimizing-the-transition-to-hizentra-for-cidp-patients-6132"
                                        
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                          
                                            Provides a dosing calculator to initiate a patient’s Hizentra therapy and to adjust the 
                                         duration of infusion based on patient need
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="02"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Counseling Tips">
                                                   
                                                
                                                      <label class="dfdd" for="02"></label> <div class="adn" > Managing Common Infusion Issues</div></label>
                                                     
                                                    <!--https://cc-oge.online/hiz-cidp-cslb-sample/managing-common-infusion-issues-6150-->
                                            </span> 
                                            <a href="https://cc-oge.online/hiz-cidp-cslb-sample/managing-common-infusion-issues-6256"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                               
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left">
                                          
                                          
                                            Offers clinical management troubleshooting techniques to possibly help resolve potential 
                                           infusion issues that may arise
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    
                            
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                   <!-- this is i implemented down -->
                                    <label for="02" class="fillable-form">
                                        Fillable Forms
                                    </label>
                                       <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="03"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Counseling Tips">
                                                    
                                                     <label class="dfdd" for="03"></label> <div class="adn" >Prescription Referral Form</div></label>
                                                      <!--<label class="dfdd" for="03"></label> <div class="adn" > Hizentra Connect<sup>SM</sup> Resource Center—Benefits Investigation Request Prescription Referral Form</div></label>-->
                                                     
                                             
                                                   
    
                                                   
                                            </span>
                                            <a href="https://cc-oge.online/hiz-cidp-cslb-sample/hizentra-connect—prescription-referral-form-6734"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                               
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                       <div class="acc__panel  mr-left">
                                           <!--  Your eligible patients may be entitled to medications for no more than
                                            $8.95/prescription, which is a Medicare Part D
                                            benefit. -->Helps patients get started on Hizentra through the Hizentra Connect resource center
                                        </div>
                                    
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="04"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Hepatic Encephalopathy Hospital Discharge Tips">
                                                <label for="04"><!-- Hepatic Encephalopathy Hospital Discharge Tips -->Free Trial Program Request Form</label>
                                            </span>
                                            <a href="https://cc-oge.online/hiz-cidp-cslb-sample/free-trial-program-request-form-6732"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left">
                                            <!-- Offers information providers should share when a patient is discharged to
                                            them from the hospital -->
                                            Helps eligible patients receive a trial of Hizentra
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                         <section class="container bg-grey">
                         <div class="education-grey">
                            Educational resources for patients about the importance of managing<br>
                      <span class="chronic-txt" >Chronic Inflammatory Demyelinating Polyneuropathy (CIDP)</span>
                        </div>
                         </section>


                     <div class="row headings " id="patients">
                        <div class="col-6">
                            <div class="mr-left jjg">
                                <h3 class="pro">PATIENT</h3>
                            </div>
                        </div>
                        <!--  <div class="col-6">
                            <small>*These resources cannot be printed.</small>
                        </div> -->
                    </div>


                 <!--    <div class="row mr-top1">
                        <div class="col-12  mr-left">
                            <div class="pd-left sub-title">
                                <h3>Fillable Forms*</h3>

                                <aside class="points ">
                                    <ul class="">
                                        <li><span>1</span>Fill in <br>
                                            form</li>
                                        <li><span>2</span>Print and/or<br>
                                            save/submit</li>
                                        <li><span>3</span>You can email<br>
                                            or copy the link</li>
                                    </ul>
                                </aside>

                            </div>
                        </div>
                    </div>
 -->
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="05">
                                                <label for="05"><!-- Letter of Medical Necessity -->My Life, My Way With Hizentra</label>
                                            </span>
                                            <a href="https://cc-oge.online/hiz-cidp-cslb-sample/my-life,-my-way-with-hizentra-6147"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                           <!--  A standard form for a patient-specific letter of medical necessity to
                                            explain your clinical decision making in choosing
                                            a therapy -->Provides patient information about CIDP, how Hizentra differs from IVIg and how it 
                                           works, and how patients can get started on Hizentra</span>
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="06">
                                               
                                                
                                                
                                                  <label class="dfdd" for="06"></label> <div class="adn" >Is Self-infused Ig Right for Your CIDP?</div></label>
                                                     
                                            </span>
                                            <a href=" https://cc-oge.online/hiz-cidp-cslb-sample/is-self-infused-ig-right-for-your-cidp-6143"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left ">
                                           <!--  Use to reduce the cost share of a medication. Follows submission of a PA or
                                            Utilization Management request. -->
                                            Offers a provider discussion guide to help patients and providers decide if subcutaneous 
                                           Ig treatment with Hizentra might be an option for them
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="07">
                                                <label for="07"><!-- CoverMyMeds<sup>®</sup> ePA* -->Hizentra Infusion Guide</label>
                                            </span>
                                            <a href="https://cc-oge.online/hiz-cidp-cslb-sample/hizentra-infusion-guide-6146"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left">
                                            <!-- Use your free login to submit ePA form. -->
                                            Provides a self-administration guide of the step-by-step instructions on preparation, 
                                             proper infusion techniques, and administration
                                            <!-- <span class="note">PLEASE NOTE: This
                                                    tool
                                                    can not be printed.</span> -->
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>

                </section>
            </section>


           <!--  <section class=" mr-top1" id="caregiver">
                <section class="container">
                    <div class="row justify-content-between align-items-center ">
                        <div class="col-12">
                            <div class="laguage-sel">

                                <p>Identify a caregiver whenever possible who will help the patient with his or her
                                    treatment plan.</p> -->

                                <!--  <p>Choose the language of the following caregiver and patient resources.</p>
                                     <ul class="tabs">
                                        <li class="active" data-tab="tab-1"> English </li>
                                        <li class=" " data-tab="tab-2"> Spanish </li>
                                    </ul> -->
<!-- 
                            </div>
                        </div>
                    </div>
                </section>
            </section> -->


          <!--   <section class="side-space main-section mr-top2 ">
                <section class="container">
                    <div id="tab-1" class="tab-content current">
                        <div class="row headings ">
                            <div class="col-md-8">
                                <div class="mr-left">
                                    <h3>Caregiver and Patient</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12  mr-left">
                                <div class="pd-left sub-title">
                                    <h3>Resources</h3>
                                </div>
                            </div>
                        </div>
 -->
                       <!--  <div class="row "> -->
                            <!--<div class="col-12 ">
                                <div class="pd-left">
                                    <div class="acc pd-left">
                                         <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="07a">
                                                    

                                                </span>
                                                <a href="{{ asset('sample/pdf/8_Copay_Card.pdf') }}"
                                                    target="_blank" class="preview">
                                                    <img src="{{asset('sample/img/search.png')}}">
                                                </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                            </div>
                                           
                                        </div> -->
                                       <!--  <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="08">
                                                  
                                                </span>
                                                <a href="{{ asset('sample/pdf/9_Reduce_Risk_of_OHE_Recurrence_PI.pdf') }}"
                                                    target="_blank" class="preview">
                                                    <img src="{{asset('sample/img/search.png')}}">
                                                </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                         

                                            </div>
                                        </div> -->

                                       <!--  <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="09">
                                                    <label for="09">Setting up Medical Alerts on Digital Devices</label>
                                                </span>
                                                <a href="{{ asset('sample/pdf/10_Setting_up_Medical_Alerts.pdf') }}"
                                                    target="_blank" class="preview">
                                                    <img src="{{asset('sample/img/search.png')}}">
                                                </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                            <div class="acc__panel  mr-left">
                                                Instructions for setting up alerts on an iPhone, Android, or Apple Watch
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
 -->
                       <!--  <div class="row mr-top1">
                            <div class="col-12  mr-left">
                                <div class="pd-left sub-title">
                                    <h3>Video</h3>

                                    <aside class="points ">
                                        <ul class="">
                                            <li><span>1</span>Viewed<br>
                                                online</li>
                                            <li><span>2</span>You can email<br>
                                                or copy the link</li>
                                        </ul>
                                    </aside>

                                </div>
                            </div>
                        </div> -->

                     
                    <div id="tab-2" class="tab-content ">
                        <div class="row headings ">
                            <div class="col-md-8">
                                <div class="mr-left">
                                    <h3>Caregiver and Patient 02</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12  mr-left">
                                <div class="pd-left sub-title">
                                    <h3>Resources 02</h3>
                                </div>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-12 ">
                                <div class="pd-left">
                                    <div class="acc pd-left">
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="07">
                                                    <label for="07">HIZENTRA Instant Savings Card

                                                        <small class="small-info">PLEASE NOTE: This is an online form
                                                            that
                                                            allows you to download the card. It is not available
                                                            in Spanish.</small>
                                                    </label>

                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                Provides most eligible, commercially insured patients help with their
                                                monthly
                                                co-pays for HIZENTRA. <span class="note">PLEASE NOTE: This tool can not
                                                    be
                                                    printed.</span>
                                            </div>
                                        </div>
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="08">
                                                    <label for="08">Reduce the Risk of OHE Recurrence With
                                                        HIZENTRA</label>
                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                            <div class="acc__panel  mr-left">
                                                Provides information about HIZENTRA coverage, prescription savings and
                                                how to
                                                sign up
                                                for the HE Living Program (H.E.L.P.).

                                            </div>
                                        </div>

                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="09">
                                                    <label for="09">Setting up Medical Alerts on Digital Devices</label>
                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                            <div class="acc__panel  mr-left">
                                                Instructions for setting up alerts on an iPhone, android or Apple Watch.
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                       
                    </div>





                </section>
                <!-- BG-BLUE added by me down -->
                <section class="BG-BLUE">
                <section class="container">
                    <a class="back-to-top">
                        Back to Top <img src="img/top.png" class="bt">
                    </a>
                    <!--   <hr class="gr-divi"> -->
                </section>


                <section class="container">
                    <div class="row ">
                        <div class="col-12 d-flex justify-content-center action">
                            <div href="#" data-toggle="modal" data-target="#myModal" class="button">Email</div>
                            <div class="button" data-toggle="modal" data-target="#print">View/Print
                            </div>
                            <div class="button" data-toggle="modal" data-target="#copy" id="copy-tool">Copy Link</div>
                        </div>
                    </div>
                </section>
                    </section>

                <section class="para">
                    <div class="row ">
                        <div class="col-12 imp-safety">
                            <h6 class="head" id="isi">Important Safety Information</h6>
                            <div class="war">WARNING: Thrombosis may occur with immune globulin products, including Hizentra. Risk 
    factors may include: advanced age, prolonged immobilization, hypercoagulable conditions, history 
    of venous or arterial thrombosis, use of estrogens, indwelling vascular catheters, hyperviscosity, 
    and cardiovascular risk factors.<br></div>
    <div class="war1">
    For patients at risk of thrombosis, administer Hizentra at the minimum dose and infusion rate 
    practicable. Ensure adequate hydration in patients before administration. Monitor for signs and 
    symptoms of thrombosis and assess blood viscosity in patients at risk for hyperviscosity.</div>
    <div class="hiz">
        Hizentra is contraindicated in patients with a history of anaphylactic or severe systemic reaction to 
human immune globulin (Ig) or components of Hizentra (eg, polysorbate 80), as well as in patients with 
immunoglobulin A deficiency with antibodies against IgA and a history of hypersensitivity. Because 
Hizentra contains L-proline as stabilizer, use in patients with hyperprolinemia is contraindicated.
    </div>
    <div class="iga">IgA-deficient patients with anti-IgA antibodies are at greater risk of severe hypersensitivity and anaphylactic 
reactions. Thrombosis may occur following treatment with Ig products, including Hizentra.</div>

<div class="mon">
    Monitor patients for aseptic meningitis syndrome (AMS), which may occur following treatment with Ig 
products, including Hizentra. In patients at risk of acute renal failure, monitor renal function, including blood 
urea nitrogen, serum creatinine and urine output. In addition, monitor patients for clinical signs of hemolysis or 
pulmonary adverse reactions (eg, transfusion-related acute lung injury [TRALI]).
</div>
<div class="cdj">Hizentra is derived from human blood. The risk of transmission of infectious agents, including viruses and, 
theoretically, the Creutzfeldt-Jakob disease (CJD) agent and its variant (vCJD), cannot be completely 
eliminated.</div>

<div class="common">The most common adverse reactions (observed in ≥5% of study subjects) were local infusion-site reactions, as 
well as headache, diarrhea, fatigue, back pain, nausea, extremity pain, cough, upper respiratory tract infection, 
rash, pruritus, vomiting, upper abdominal pain, migraine, arthralgia, pain, fall, and nasopharyngitis.</div>
<div class="pass">The passive transfer of antibodies can interfere with response to live virus vaccines and lead to 
misinterpretation of serologic test results.</div>

<div class="ple">Please see full <a class="pre" target="_blank" href="https://labeling.cslbehring.com/PI/US/Hizentra/EN/Hizentra-Prescribing-Information.pdf">Prescribing Information</a> for Hizentra including boxed warning.</div>

        <div class="rep">To report SUSPECTED ADVERSE REACTIONS, contact the CSL Behring Pharmacovigilance Department at 
<span class="num" >1-866-915-6958</span> or FDA at <span class="num1">1-800-FDA-1088</span> or <a class="med" target="_blank" href="https://www.fda.gov/medwatch">www.fda.gov/medwatch</a>.</div>



<div class="logo123" style="padding">
    <img src="{{asset('sample/img/foo.png')}}">
    <div class="con">
     Questions?<a class="con1" href="http://go.aventriahealth.com/ScigQsaSupportContactUs.html" target="_blank">Contact Us</a>
</div>
</div>




<div class="foo">
    Hizentra is manufactured by CSL Behring AG and distributed by CSL Behring LLC. <br>
Hizentra<sup>®</sup> is a registered trademark of CSL Behring AG.<br>
Biotherapies for Life<sup>®</sup> is a registered trademark of CSL Behring LLC.<br>
<span>Hizentra Connect<sup>SM</sup> is a service mark of CSL Behring LLC.</span><br>
<span class="foo2">
©2021 CSL Behring LLC.</span> The product information presented on this site is intended for US residents only. HIZ-0289-AUG21
</div>
<div class="foo1">
   <a class="la1" target="_blank" href="https://www.cslbehring.com/"> CSLBehring.com</a> | <a class="la2" target="_blank" href="https://www.hizentra.com/">Hizentra.com</a> 
</div>


                        </div>
                    </div>
                </section>
            </section>


        </form>
        <!-- Main Section Over-->

        <div class="copy-cbd  alert-success">Copy to Clipboard</div>
        <div class="uncopy-cbd alert-danger">Checkbox is not selected</div>


        <!-- Footer Starts -->
      <!--   <footer class="side-space text-center">
            <section class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="bgfoo-Container">
                            <div class="CSL Behring-logo"><img src="img/CSL Behring-logo.png"></div>
                            <div class="copyright">© 2020 CSL Behring Pharmaceuticals or its affiliates. .0193.USA.19</div>
                            
                            
                        </div>
                    </div>
                </div>
            </section>
        </footer> -->
        <!-- Footer Ends -->
    </div>

    <!-- Modal Starts here-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to email the selected resource link
                        to a patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!--  <div class="modal-body">
                   
                </div> -->
            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->

    <!-- Modal Starts here-->
    <div id="print" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to print the selected tool.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->

    <!-- Modal Starts here-->
    <div id="copy" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to copy the link for the selected
                        resource to email to a
                        patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->







    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/sample/plugins.js')}}"></script>
    <script src="{{asset('js/sample/main.js')}}"></script>

</body>

</html>
Interested Items <br>
Quick Support = {{ $form->quick_support }},<br>
Order Set Kit = {{ $form->order_set_kit }},<br>

Information<br>
Name = {{ $form->name }},<br>
Email = {{ $form->email }},<br>
Professional Title = {{ $form->professional_title }},<br>
Organization Name = {{ $form->name_of_organization }},<br>
Organization City = {{ $form->organization_city }},<br>
Organization State = {{ $form->organization_state }},<br>
Type of Organization = {{ $form->type_of_organization }}<br>

Thanks
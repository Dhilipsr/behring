@extends('layout.layout')

@section('title')
 Hizentra QSA - Quick Support and Access
@endsection

@section('css')
 <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="mainBody">
        <div class="grey-bac">
            <section class="container">
                <h6 class="main-topline">
                 {!! $content->section_one_content !!}
                 
                </h6>
                
                <div  class="quick-support" >
                    {!! $content->section_two_content !!}
                      
                </div>
             </section>
        </div>
        <section class="container" id="isi">
            <div class="row">

               <!--  <section class="grey-bac"> -->
                <div class="col-12">
                    <!--  <div class="grade-logo"><img src="img/grade.png"></div> -->
             <div class="col-12">
             
          </div>
                </div>

               <div class="col-12"><div class="saf">Important Safety Information</div>

                <div class="col-12">
                    <div class="content">
                        <span class="con-tw">

                        <!--{!! $content->section_two_content !!}-->
                    </span>
                    </div>
                    <div class="imp-content">

                        <h6 class="ind">Indications</h6>
                        <p>Hizentra, Immune Globulin Subcutaneous (Human), 20% Liquid, is indicated for:</p>
                        <ul>
                          <li>
                       <p>Treatment of primary immunodeficiency (PI) in adults and pediatric patients 2 years and older.</p>
                      </li>
                      <li>
                       <p>Maintenance therapy in adults with chronic inflammatory demyelinating polyneuropathy (CIDP) to 
                       prevent relapse of neuromuscular disability and impairment.</p>
                       </li>
                       </ul>
                       
                        <p style="padding-left: 20px; color:#778288;text-indent: -17px;">
                            
                       – Limitation of use: maintenance therapy in CIDP has been systematically studied for 6 months and for a further 12 months in a follow-up study. Continued maintenance beyond these periods should be individualized based on patient response and need for continued therapy.
                       </p>
                       
                           
                           
                        <p class="sub" style="padding-top:10px;">For subcutaneous infusion only.</p>
                        <p class="sub"> "WARNING: Thrombosis may occur with immune globulin products, including Hizentra. Risk factors may include: advanced age, prolonged immobilization, hypercoagulable conditions, history of venous or arterial thrombosis, use of estrogens, indwelling vascular catheters, hyperviscosity, and cardiovascular risk factors."</p>
                        <p class="sub">For patients at risk of thrombosis, administer Hizentra at the minimum dose and infusion rate
                         practicable. Ensure adequate hydration in patients before administration. Monitor for signs and
                        symptoms of thrombosis and assess blood viscosity in patients at risk for hyperviscosity.</p>
                        <p class="add">Please see additional Important Safety Information <a class="bel" data-scroll href="#note">below</a> and <a class="cli" href="https://labeling.cslbehring.com/PI/US/Hizentra/EN/Hizentra-Prescribing-Information.pdf" target="_blank">click
                                here</a> for full Prescribing Information.</p>
                    </div>

                    {{-- <div class="imp-content">
                         {!! $content->section_three_title !!}
                    </div> --}}
                </div>
            </div>

        </section>

    </div>

    <div class="mainBody sec-btm">
        <section class="container">
            <div class="row flex-sm-row-reverse">

                <div class="col-md-6">
                    <div class="content">
                        <div class="con_one" style="">
                         {!! $pluginContent->content_one !!}
                     </div>
                         <ul class="buttons bdr">
                            <li><a href="#" class="btn d-blue gaClick" data-toggle="modal" data-target="#myModal" data-label="hizentra Support Order">Order Plugin</a></li>
                            <li><a href="https://www.research.net/r/SCIGQSA" class="btn gaClick" target="_blank" data-label="GINA">Ask GINA&trade;</a></li>
                            
                        </ul>
                        {!! $pluginContent->content_two !!}
                        <ul class="buttons bdr">
                           
                          <li><a href="#" class="btn gaClick" data-toggle="modal" data-target="#myModal" data-label="hizentra Support Order">Order Set Kit</a>
                            </li>
                             <li><a href="https://cc-oge.online/hiz-cslb-master/leave-behind-6236" target="_blank" class="btn gaClick" data-label="Flashcard">View Leave-behind</a>
                            </li>

                        </ul>

                        <p class="note">GINA is a trademark of Aventria Health Group.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="img-sec">
                        <a href="{{route('samplePlugin')}}"  target="_blank" class="gaClick" data-label="Sample"><img src="img/samp1.png"></a>
                    </div>

                    <ul class="buttons bdr text-center" id="de">
                        <li><a href="{{route('samplePlugin')}}" target="_blank" class="btn gaClick" data-label="Sample">View Live Sample</a>
                        </li>

                    </ul>
                </div>
            </div>
        </section>

    </div>
    <div id="here"></div>
     <div class="mainBody" id="note">
        <section class="container"id="#imnote">
            <div class="row" >

                <div class="col-12">


                    <div class="imp-content" >
                        <h6 id="note1">Important Safety Information (Continued)</h6>


                        <p>Hizentra is contraindicated in patients with a history of anaphylactic or severe systemic reaction to human 
immune globulin (Ig) or components of Hizentra (eg, polysorbate 80), as well as in patients with immunoglobulin 
A deficiency with antibodies against IgA and a history of hypersensitivity. Because Hizentra contains L-proline as 
stabilizer, use in patients with hyperprolinemia is contraindicated.</p>

        <p>IgA-deficient patients with anti-IgA antibodies are at greater risk of severe hypersensitivity and anaphylactic 
reactions. Thrombosis may occur following treatment with Ig products, including Hizentra.</p>
        <p>Monitor patients for aseptic meningitis syndrome (AMS), which may occur following treatment with Ig products, 
including Hizentra. In patients at risk of acute renal failure, monitor renal function, including blood urea nitrogen, 
serum creatinine and urine output. In addition, monitor patients for clinical signs of hemolysis or pulmonary 
adverse reactions (eg, transfusion-related acute lung injury [TRALI]).</p>

<p>
    Hizentra is derived from human blood. The risk of transmission of infectious agents, including viruses and, 
theoretically, the Creutzfeldt-Jakob disease (CJD) agent and its variant (vCJD), cannot be completely eliminated.
</p>
<p>
    The most common adverse reactions (observed in ≥5% of study subjects) were local infusion-site reactions, as 
well as headache, diarrhea, fatigue, back pain, nausea, extremity pain, cough, upper respiratory tract infection, 
rash, pruritus, vomiting, upper abdominal pain, migraine, arthralgia, pain, fall, and nasopharyngitis.
</p>
<p>
    The passive transfer of antibodies can interfere with response to live virus vaccines and lead to misinterpretation 
of serologic test results.
</p>
                       <!--  <h6>IMPORTANT SAFETY INFORMATION</h6> -->
                       <!--  <ul class="mb-4"

                        </ul> -->
                           <p><strong>Please see full <a class="ing" href="https://labeling.cslbehring.com/PI/US/Hizentra/EN/Hizentra-Prescribing-Information.pdf" target="_blank">Prescribing Information</a> for Hizentra including boxed warning.</strong></p>

                        <p class="rep-12" style="font-weight: 500;">
                            To report SUSPECTED ADVERSE REACTIONS, contact the CSL Behring Pharmacovigilance Department at<br> <a class="ing" 
                                href="tel:1-866-915-6958">1-866-915-6958</a> or FDA at <a class="ing" 
                                href="tel:1-800-FDA-1088">1-800-FDA-1088</a> or <a class="ing" href="https://www.fda.gov/medwatch"
                                target="_black">www.fda.gov/medwatch</a>.</p>
                     
                    </div>
                    
                
                   <!--  <div class="imp-content ref">
                        <p><strong>References: 1.</strong> Ehringer G, Duffy B. Promoting best practice and safety
                            through preprinted physician orders. In:
                            Henriksen K, Battles JB, Keyes MA, et al, eds.<i> Advances in Patient Safety: New Directions
                                and Alternative Approaches
                                (Vol. 2: Culture and Redesign)</i>. Rockville, MD: Agency for Healthcare Research and
                            Quality; 2008.</p>
                    </div> -->
                </div>
            </div>

        </section>

    </div>
@endsection

@section('js')
  <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script type="text/javascript">

$("#email-form").validate({
    rules:{
        type_of_organization:{
                required:true
            },
        tnc:{
            required:true
        },
          quick_support:{
            required:true
        }
    },
        messages: {
            name: "Please enter your name",
            email: "Please enter your email",
            professional: "Please enter your professional title",
            organization: "Please enter your organization's name",
            city: "Please enter your organization's city",
            state: "Please enter your organization’s state",
            type_of_organization:"Please select at least one type of organization",
            tnc:"Please certify you are a healthcare provider",
             quick_support:"Please select at least one checkbox"

        }
    });
    var quick_support; 
          $('#quick_support').on('change', function() {
                 quick_support = this.checked ? this.value : '';
                  
           console.log(quick_support);
                });
     
      var order_set_kit; 
          $('#order_set_kit').on('change', function() {
                 order_set_kit = this.checked ? this.value : '';
                  
           console.log(order_set_kit);
                });

    $(document).on('click','#send_email', function(e){
        // get the values
       
        var name = $('#name').val();
        var email = $('#email').val();
        var professional_title = $('#professional_title').val();
        var name_of_organization = $('#name_of_organization').val();
        var organization_city = $('#organization_city').val();
        var organization_state = $('#organization_state').val();
         var manager_name = $('#manager_name').val();
        var type_of_organization = $("input[name='type_of_organization']:checked").val();
        if(type_of_organization == 'other')
        {
            type_of_organization = $("#other").val();
        }
        var tandc = $('#tandc').val();
        // console.log(quick_support,order_set_kit,name,email,professional_title,name_of_organization,organization_city,organization_state,type_of_organization);
       
      

        if($('#email-form').valid())
        {
           
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                } 
            });
            $.ajax({
              
                url : "{{route('plugin.form.submit')}}",
                type: "POST",
                data : {'quick_support' : quick_support, 'order_set_kit' : order_set_kit, 'name' : name, 'email' : email, 'professional_title' : professional_title, 'name_of_organization' : name_of_organization, 'organization_city' : organization_city, 'organization_state' : organization_state,'manager_name': manager_name, 'type_of_organization' : type_of_organization},
                success: function(response)
                {   
                     console.log(response)
                     if(response.status == 200) {
                   
                  
                        $('#myModal').removeClass('show');
                        $('#myModal-2').addClass('show');
                        $('#myModal-2').css('display','block');
                     }
                    // return (response)
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
             
                }
            });
        }
        
        e.preventDefault();
    });
function myFunction() {
  $('.close').on('click', function () {
            $('.modal-backdrop').remove();
            $(this).parents('#myModal-2').hide();
            location.reload();
        })
}
// $('.gaClick').click(function(e){
//             // google analytics click functionality
//           var label = $(this).attr('data-label');
//           var test = ga('send', {
//              hitType: 'event',
//              eventCategory: label,
//              eventAction:  label,
//              eventLabel: label
//           });

//     });
// $('.gaPi').click(function(e){
//             // google analytics click functionality
//           var label = 'PI';
//           var test = ga('send', {
//              hitType: 'event',
//              eventCategory: 'PI',
//              eventAction:  label,
//              eventLabel: label
//           });

//     });
// $('.gaIsi').click(function(e){
//             // google analytics click functionality
//           var label = 'ISI';
//           var test = ga('send', {
//              hitType: 'event',
//              eventCategory: 'ISI',
//              eventAction:  label,
//              eventLabel: label
//           });

//     });
</script>
<script>
    var position = $(window).scrollTop(); 

// should start at 0

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if(scroll > position) {
        console.log('scrollDown');
        // $('div').text('Scrolling Down Scripts');
    } else {
         console.log('scrollUp');
        //  document.getElementById("ide").style.margin-top:"3%";
        //  $('div').text('Scrolling Up Scripts');
        
    }
    position = scroll;
});
</script>
@endsection
-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 01, 2022 at 05:02 AM
-- Server version: 5.7.37
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hizentra_web_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `ctas`
--

CREATE TABLE `ctas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 9),
(22, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 5, 'section_one_content', 'rich_text_box', 'Section One Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(24, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(25, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(26, 5, 'section_two_content', 'rich_text_box', 'Section Two Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(27, 5, 'section_three_title', 'rich_text_box', 'Section Three Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(28, 5, 'section_four_content', 'rich_text_box', 'Section Four Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(29, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(30, 7, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(31, 7, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"slugify\":{\"origin\":\"title\"}}', 3),
(32, 7, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(33, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(34, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(35, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 9, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(37, 9, 'content_one', 'rich_text_box', 'Content One', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(38, 9, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(39, 9, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(40, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(41, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(42, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(43, 10, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 4),
(44, 10, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 5),
(45, 10, 'professional_title', 'text', 'Professional Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(46, 10, 'name_of_organization', 'text', 'Name Of Organization', 0, 1, 1, 1, 1, 1, '{}', 7),
(47, 10, 'organization_city', 'text', 'Organization City', 0, 1, 1, 1, 1, 1, '{}', 8),
(48, 10, 'organization_state', 'text', 'Organization State', 0, 1, 1, 1, 1, 1, '{}', 9),
(49, 10, 'type_of_organization', 'text', 'Type Of Organization', 0, 1, 1, 1, 1, 1, '{}', 10),
(50, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(51, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(52, 10, 'quick_support', 'text', 'Quick Support', 0, 1, 1, 1, 1, 1, '{}', 2),
(53, 10, 'order_set_kit', 'text', 'Order Set Kit', 0, 1, 1, 1, 1, 1, '{}', 3),
(54, 10, 'manager_name', 'text', 'Manager Name', 0, 1, 1, 1, 1, 1, '{}', 10),
(55, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 5);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-04-01 23:59:57', '2021-10-09 06:53:57'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-04-01 23:59:58', '2020-04-01 23:59:58'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-04-01 23:59:58', '2020-04-01 23:59:58'),
(5, 'home_pages', 'home-pages', 'Home Page', 'Home Pages', 'voyager-dot', 'App\\HomePage', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(7, 'ctas', 'ctas', 'Cta', 'Ctas', 'voyager-dot', 'App\\Cta', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(9, 'plugins', 'plugins', 'Plugin', 'Plugins', 'voyager-dot', 'App\\Plugin', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-02 00:30:34', '2020-04-29 05:20:06'),
(10, 'plugin_orders', 'plugin-orders', 'Plugin Order', 'Plugin Orders', 'voyager-dot', 'App\\PluginOrder', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-15 05:04:24', '2020-09-02 04:42:28');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_pages`
--

CREATE TABLE `home_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `section_one_content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `section_two_content` longtext COLLATE utf8mb4_unicode_ci,
  `section_three_title` longtext COLLATE utf8mb4_unicode_ci,
  `section_four_content` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_pages`
--

INSERT INTO `home_pages` (`id`, `section_one_content`, `created_at`, `updated_at`, `section_two_content`, `section_three_title`, `section_four_content`) VALUES
(1, '<p>Integrate Care for Your Patients With CIDP</p>', '2020-04-02 00:25:00', '2021-10-12 16:44:34', '<p>With the Hizentra<sup>&reg;</sup> Quick Support &amp; Access Plugin, you can integrate important dosing, reminder instructions, and patient hub and education resources into your organization&rsquo;s workflow and have these resources at the ready when you are meeting with a patient with chronic inflammatory demyelinating polyneuropathy (CIDP), whether in person or virtually.</p>', '<p>a</p>', '<p>a</p>');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-04-01 23:59:59', '2020-04-01 23:59:59');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-04-02 00:00:00', '2020-04-02 00:00:00', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 9, '2020-04-02 00:00:00', '2020-04-02 00:36:46', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 5, 8, '2020-04-02 00:00:00', '2020-04-02 00:36:44', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 5, 6, '2020-04-02 00:00:00', '2020-04-02 00:36:40', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 2, '2020-04-02 00:00:00', '2020-04-02 00:36:46', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-04-02 00:00:00', '2020-04-02 00:35:43', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-04-02 00:00:01', '2020-04-02 00:35:43', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-04-02 00:00:01', '2020-04-02 00:35:43', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-04-02 00:00:01', '2020-04-02 00:35:43', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, 5, 7, '2020-04-02 00:00:01', '2020-04-02 00:36:41', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-04-02 00:00:05', '2020-04-02 00:35:43', 'voyager.hooks', NULL),
(12, 1, 'Home Pages', '', '_self', 'voyager-dot', NULL, 1, 1, '2020-04-02 00:15:05', '2020-04-02 00:36:14', 'voyager.home-pages.index', NULL),
(14, 1, 'Ctas', '', '_self', 'voyager-dot', NULL, 1, 2, '2020-04-02 00:23:42', '2020-04-02 00:36:32', 'voyager.ctas.index', NULL),
(16, 1, 'Plugins', '', '_self', 'voyager-dot', NULL, 1, 3, '2020-04-02 00:30:35', '2020-04-02 00:36:37', 'voyager.plugins.index', NULL),
(18, 1, 'Plugin Orders', '', '_self', 'voyager-dot', NULL, NULL, 3, '2020-04-15 05:04:25', '2020-04-15 05:04:25', 'voyager.plugin-orders.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_01_01_000000_add_voyager_user_fields', 1),
(3, '2016_01_01_000000_create_data_types_table', 1),
(4, '2016_05_19_173453_create_menu_table', 1),
(5, '2016_10_21_190000_create_roles_table', 1),
(6, '2016_10_21_190000_create_settings_table', 1),
(7, '2016_11_30_135954_create_permission_table', 1),
(8, '2016_11_30_141208_create_permission_role_table', 1),
(9, '2016_12_26_201236_data_types__add__server_side', 1),
(10, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(11, '2017_01_14_005015_create_translations_table', 1),
(12, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(13, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(14, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(15, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(16, '2017_08_05_000000_add_group_to_settings_table', 1),
(17, '2017_11_26_013050_add_user_role_relationship', 1),
(18, '2017_11_26_015000_create_user_roles_table', 1),
(19, '2018_03_11_000000_add_user_settings', 1),
(20, '2018_03_14_000000_add_details_to_data_types_table', 1),
(21, '2018_03_16_000000_make_settings_value_nullable', 1),
(22, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(2, 'browse_bread', NULL, '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(3, 'browse_database', NULL, '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(4, 'browse_media', NULL, '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(5, 'browse_compass', NULL, '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(6, 'browse_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(7, 'read_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(8, 'edit_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(9, 'add_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(10, 'delete_menus', 'menus', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(11, 'browse_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(12, 'read_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(13, 'edit_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(14, 'add_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(15, 'delete_roles', 'roles', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(16, 'browse_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(17, 'read_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(18, 'edit_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(19, 'add_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(20, 'delete_users', 'users', '2020-04-02 00:00:02', '2020-04-02 00:00:02'),
(21, 'browse_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(22, 'read_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(23, 'edit_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(24, 'add_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(25, 'delete_settings', 'settings', '2020-04-02 00:00:03', '2020-04-02 00:00:03'),
(26, 'browse_hooks', NULL, '2020-04-02 00:00:06', '2020-04-02 00:00:06'),
(27, 'browse_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(28, 'read_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(29, 'edit_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(30, 'add_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(31, 'delete_home_pages', 'home_pages', '2020-04-02 00:15:05', '2020-04-02 00:15:05'),
(37, 'browse_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(38, 'read_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(39, 'edit_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(40, 'add_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(41, 'delete_ctas', 'ctas', '2020-04-02 00:23:42', '2020-04-02 00:23:42'),
(47, 'browse_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(48, 'read_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(49, 'edit_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(50, 'add_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(51, 'delete_plugins', 'plugins', '2020-04-02 00:30:35', '2020-04-02 00:30:35'),
(52, 'browse_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(53, 'read_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(54, 'edit_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(55, 'add_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24'),
(56, 'delete_plugin_orders', 'plugin_orders', '2020-04-15 05:04:24', '2020-04-15 05:04:24');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1);

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_one` longtext COLLATE utf8mb4_unicode_ci,
  `content_two` longtext COLLATE utf8mb4_unicode_ci,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `image`, `content_one`, `content_two`, `link`, `created_at`, `updated_at`) VALUES
(1, 'plugins/September2021/3H9mQxanpLJgu2m0ZqMH.png', '<h2>Help Your Patients Get Support and Access to Their Prescribed Treatment</h2>\r\n<div style=\"color: #778282;\">\r\n<p style=\"color: #778282;\">Give your patients and their caregivers tools and resources to help them:</p>\r\n<ul>\r\n<li>Understand CIDP and how Hizentra may help&nbsp;</li>\r\n<li>Determine if self-infusions are right for them</li>\r\n<li>Get financial support for their prescription</li>\r\n</ul>\r\n<p class=\"cin\">GINA&trade; (Guided INtegration Assistant) can help you integrate the plugin into your EHR system.</p>\r\n</div>', '<p>With a few keystrokes or mouse clicks, you can be sure you&rsquo;re following your organization&rsquo;s guideline-driven protocols for CIDP.&nbsp;&nbsp;</p>\r\n<ul>\r\n<li>The Order Set Kit has [4] components.</li>\r\n<li>See page [4] of the Hizentra QSA Leave-behind for details.</li>\r\n</ul>', 'test', '2020-04-02 00:31:00', '2021-11-23 09:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `plugin_orders`
--

CREATE TABLE `plugin_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `quick_support` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_set_kit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `professional_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_of_organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manager_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plugin_orders`
--

INSERT INTO `plugin_orders` (`id`, `quick_support`, `order_set_kit`, `name`, `email`, `professional_title`, `name_of_organization`, `organization_city`, `organization_state`, `manager_name`, `type_of_organization`, `created_at`, `updated_at`) VALUES
(462, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:14:57', '2021-10-09 08:14:57'),
(463, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:14:57', '2021-10-09 08:14:57'),
(464, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:16:36', '2021-10-09 08:16:36'),
(465, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:16:42', '2021-10-09 08:16:42'),
(466, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:00', '2021-10-09 08:17:00'),
(467, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:01', '2021-10-09 08:17:01'),
(468, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:02', '2021-10-09 08:17:02'),
(469, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:03', '2021-10-09 08:17:03'),
(470, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:03', '2021-10-09 08:17:03'),
(471, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:05', '2021-10-09 08:17:05'),
(472, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:06', '2021-10-09 08:17:06'),
(473, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:07', '2021-10-09 08:17:07'),
(474, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:07', '2021-10-09 08:17:07'),
(475, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:08', '2021-10-09 08:17:08'),
(476, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:09', '2021-10-09 08:17:09'),
(477, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:10', '2021-10-09 08:17:10'),
(478, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:11', '2021-10-09 08:17:11'),
(479, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:12', '2021-10-09 08:17:12'),
(480, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:12', '2021-10-09 08:17:12'),
(481, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:13', '2021-10-09 08:17:13'),
(482, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:14', '2021-10-09 08:17:14'),
(483, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:15', '2021-10-09 08:17:15'),
(484, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:16', '2021-10-09 08:17:16'),
(485, 'yes', 'no', 'test', 'testxxxpower@yopmail.com', 'dsd', 'adad', 'asdas', 'adasd', 'dasd', 'asdasd', '2021-10-09 08:17:17', '2021-10-09 08:17:17'),
(487, 'no', 'yes', 'vxcvxz', 'testxxx123@yopmail.com', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 'sfsaf', 'Large Group Practice or Physician Group', '2021-10-09 08:24:30', '2021-10-09 08:24:30'),
(488, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:03', '2021-10-09 09:03:03'),
(489, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:26', '2021-10-09 09:03:26'),
(490, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:27', '2021-10-09 09:03:27'),
(491, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:27', '2021-10-09 09:03:27'),
(492, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:28', '2021-10-09 09:03:28'),
(493, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:28', '2021-10-09 09:03:28'),
(494, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:31', '2021-10-09 09:03:31'),
(495, 'yes', 'no', 'dad', 'testxxxpower@yopmail.com', 'sdfsdfs', 'sdfsd', 'sadasd', 'asdas', 'adasd', 'Large Group Practice or Physician Group', '2021-10-09 09:03:32', '2021-10-09 09:03:32'),
(496, 'yes', 'no', 'sdsad', 'testxxxpower@yopmail.com', 'adad', 'sfasss', 'sfsf', 'sfsdf', 'sdfsd', 'Health System', '2021-10-09 09:05:07', '2021-10-09 09:05:07'),
(497, 'yes', 'no', 'sdsad', 'testxxxpower@yopmail.com', 'adad', 'sfasss', 'sfsf', 'sfsdf', 'sdfsd', 'Health System', '2021-10-09 09:10:47', '2021-10-09 09:10:47'),
(498, 'yes', 'no', 'sdsad', 'testxxxpower@yopmail.com', 'adad', 'sfasss', 'sfsf', 'sfsdf', 'sdfsd', 'Health System', '2021-10-09 09:10:47', '2021-10-09 09:10:47'),
(499, 'yes', 'no', 'cvcxv', 'testxxxpower@yopmail.com', 'xczxc', 'zZcz', 'cxzz', 'gfdsfgs', 'dgdfg', 'Large Group Practice or Physician Group', '2021-10-09 09:12:18', '2021-10-09 09:12:18'),
(500, 'no', 'yes', 'dsfasfd', 'testxxxpower@yopmail.com', 'fsadf', 'sfsdafs', 'sfsad', 'sfsdaf', 'sfsadf', 'Health System', '2021-10-09 09:14:40', '2021-10-09 09:14:40'),
(501, 'yes', 'no', 'dfdf', 'testxxxpower@yopmail.com', 'sfsdf', 'fsdf', 'fsfs', 'dsfsf', 'fsdfsdf', 'Health System', '2021-10-09 09:37:21', '2021-10-09 09:37:21'),
(502, 'yes', 'no', 'dfdf', 'testxxxpower@yopmail.com', 'sfsdf', 'fsdf', 'fsfs', 'dsfsf', 'fsdfsdf', 'Health System', '2021-10-09 09:38:06', '2021-10-09 09:38:06'),
(503, 'yes', 'no', 'dfdf', 'testxxxpower@yopmail.com', 'sfsdf', 'fsdf', 'fsfs', 'dsfsf', 'fsdfsdf', 'Health System', '2021-10-09 09:38:17', '2021-10-09 09:38:17'),
(504, 'yes', 'no', 'dfdf', 'testxxxpower@yopmail.com', 'sfsdf', 'fsdf', 'fsfs', 'dsfsf', 'fsdfsdf', 'Health System', '2021-10-09 09:38:18', '2021-10-09 09:38:18'),
(505, 'yes', 'no', 'dfdf', 'testxxxpower@yopmail.com', 'sfsdf', 'fsdf', 'fsfs', 'dsfsf', 'fsdfsdf', 'Health System', '2021-10-09 09:38:18', '2021-10-09 09:38:18'),
(506, 'yes', 'no', 'dfdf', 'testxxxpower@yopmail.com', 'sfsdf', 'fsdf', 'fsfs', 'dsfsf', 'fsdfsdf', 'Health System', '2021-10-09 09:38:19', '2021-10-09 09:38:19'),
(507, 'no', 'yes', 'dadad', 'testxxxpower@yopmail.com', 'sfsdf', 'sfsdfs', 'sdfsdfs', 'sfsdf', 'sfsdf', 'Health System', '2021-10-09 09:40:41', '2021-10-09 09:40:41'),
(508, 'yes', 'no', 'fsdfs', 'testxxxpower@yopmail.com', 'fsfsa', 'sfdsf', 'fsdf', 'fsdfds', 'sfsdf', 'Large Group Practice or Physician Group', '2021-10-09 10:51:21', '2021-10-09 10:51:21'),
(509, 'yes', 'no', 'fsdfs', 'testxxxpower@yopmail.com', 'fsfsa', 'sfdsf', 'fsdf', 'fsdfds', 'sfsdf', 'Large Group Practice or Physician Group', '2021-10-09 10:54:04', '2021-10-09 10:54:04'),
(510, 'yes', 'no', 'czzcz', 'testxxxpower@yopmail.com', 'fsdf', 'sfsdf', 'sfsdf', 'sfsd', 'sfsdf', 'Large Group Practice or Physician Group', '2021-10-09 10:59:55', '2021-10-09 10:59:55'),
(511, 'yes', 'no', 'fddsfs', 'testxxxpower@yopmail.com', 'sfsdf', 'sfsd', 'fsdfs', 'sfsd', 'sdfsd', 'Health System', '2021-10-09 11:03:01', '2021-10-09 11:03:01'),
(512, 'yes', 'no', 'fddsfs', 'testxxxpower@yopmail.com', 'sfsdf', 'sfsd', 'fsdfs', 'sfsd', 'sdfsd', 'Health System', '2021-10-09 11:10:44', '2021-10-09 11:10:44'),
(513, 'yes', 'no', 'dfdsfs', 'testxxxpower@yopmail.com', 'dsfsd', 'sfsd', 'sfsd', 'sdfds', 'sfsdfds', 'Large Group Practice or Physician Group', '2021-10-09 11:16:02', '2021-10-09 11:16:02'),
(514, 'yes', 'no', 'fsdfs', 'testxxxpower@yopmail.com', 'sdfsd', 'fsdfs', 'fsdf', 'sfsf', 'sfsdf', 'Large Group Practice or Physician Group', '2021-10-09 11:23:09', '2021-10-09 11:23:09'),
(515, 'yes', 'no', 'fsdfs', 'testxxxpower@yopmail.com', 'sdfsd', 'fsdfs', 'fsdf', 'sfsf', 'sfsdf', 'Large Group Practice or Physician Group', '2021-10-09 11:24:12', '2021-10-09 11:24:12'),
(516, 'no', 'yes', 'dds', 'testxxxpower@yopmail.com', 'zdfd', 'sfsdf', 'sfsdf', 'sfsdf', 'sfdsfdsfs', 'Large Group Practice or Physician Group', '2021-10-09 11:39:17', '2021-10-09 11:39:17'),
(517, 'yes', 'no', 'dfdsfd', 'testxxxpower@yopmail.com', 'sdfdsf', 'fdsfs', 'fsdfs', 'fsdfds', 'sfsdf', 'Large Group Practice or Physician Group', '2021-10-09 11:47:26', '2021-10-09 11:47:26'),
(518, 'yes', 'no', 'cbxb', 'testxxxpower@yopmail.com', 'sfsf', 'sfsfsd', 'sfsdf', 'sfsadf', 'sfsf', 'Large Group Practice or Physician Group', '2021-10-09 11:49:05', '2021-10-09 11:49:05'),
(519, 'yes', 'no', 'vbnv', 'testxxxpower@yopmail.com', 'dasds', 'fsdsd', 'sdfsdf', 'sfsdfs', 'sfsdfsd', 'Large Group Practice or Physician Group', '2021-10-09 11:53:59', '2021-10-09 11:53:59'),
(520, 'yes', 'no', 'vbnv', 'testxxxpower@yopmail.com', 'dasds', 'fsdsd', 'sdfsdf', 'sfsdfs', 'sfsdfsd', 'Large Group Practice or Physician Group', '2021-10-09 11:54:39', '2021-10-09 11:54:39'),
(521, 'yes', 'no', 'asdas', 'testxxxpower@yopmail.com', 'sdfdsf', 'sfsdfs', 'sfsdfsd', 'sfdfsdf', 'dfsdfsd', 'Large Group Practice or Physician Group', '2021-10-09 11:58:13', '2021-10-09 11:58:13'),
(522, 'yes', 'no', 'asdas', 'testxxxpower@yopmail.com', 'sdfdsf', 'sfsdfs', 'sfsdfsd', 'sfdfsdf', 'dfsdfsd', 'Large Group Practice or Physician Group', '2021-10-09 11:58:41', '2021-10-09 11:58:41'),
(523, 'yes', 'no', 'fsdfs', 'testxxxpower@yopmail.com', 'sfsdf', 'fsdfsd', 'sfsdfsf', 'sfsfsd', 'fsdfds', 'Large Group Practice or Physician Group', '2021-10-09 12:03:23', '2021-10-09 12:03:23'),
(524, 'yes', 'no', 'sdfsdf', 'testxxxpower@yopmail.com', 'sdfsdf', 'sfsdf', 'sdfsdf', 'sfsdfs', 'sfdsfs', 'Large Group Practice or Physician Group', '2021-10-09 12:08:51', '2021-10-09 12:08:51'),
(525, 'yes', 'no', 'sdfsdf', 'testxxxpower@yopmail.com', 'sdfsdf', 'sfsdf', 'sdfsdf', 'sfsdfs', 'sfdsfs', 'Large Group Practice or Physician Group', '2021-10-09 12:08:52', '2021-10-09 12:08:52'),
(526, 'yes', 'no', 'dfsd', 'testxxx12@yopmail.com', 'dfsd', 'sfsdf', 'sfsdf', 'fasdfs', 'fsdfs', 'Large Group Practice or Physician Group', '2021-10-10 04:57:02', '2021-10-10 04:57:02'),
(527, 'yes', 'no', 'dfsd', 'testxxx12@yopmail.com', 'dfsd', 'sfsdf', 'sfsdf', 'fasdfs', 'fsdfs', 'Large Group Practice or Physician Group', '2021-10-10 04:57:46', '2021-10-10 04:57:46'),
(528, 'yes', 'no', 'dfsdf', 'testxxx@yopmail.com', 'fsdf', 'sdfsdf', 'sfsdf', 'sfsdf', 'fsdfs', 'Large Group Practice or Physician Group', '2021-10-10 05:07:20', '2021-10-10 05:07:20'),
(529, 'yes', 'no', 'fgh', 'testxxx@yopmail.com', 'dsfs', 'sfsdf', 'fsfs', 'fdsfs', 'fsfsd', 'Large Group Practice or Physician Group', '2021-10-10 05:45:56', '2021-10-10 05:45:56'),
(530, 'yes', 'no', 'sdasda', 'testxxx@yopmail.com', 'dasd', 'das', 'adasd', 'adasd', 'asdasd', 'Large Group Practice or Physician Group', '2021-10-10 06:00:53', '2021-10-10 06:00:53'),
(531, 'yes', 'no', 'sds', 'testxxx@yopmail.com', 'ZxZX', 'zxZX', 'xZX', 'XZX', 'zxZ', 'Health System', '2021-10-10 06:02:41', '2021-10-10 06:02:41'),
(532, 'yes', 'no', 'dasd', 'testxxx@yopmail.com', 'fdfs', 'fsd', 'fdf', 'sdfsdf', 'fdfd', 'Large Group Practice or Physician Group', '2021-10-10 06:12:21', '2021-10-10 06:12:21'),
(533, 'yes', 'no', 'dasd', 'testxxx@yopmail.com', 'fdfs', 'fsd', 'fdf', 'sdfsdf', 'fdfd', 'Large Group Practice or Physician Group', '2021-10-10 06:12:46', '2021-10-10 06:12:46'),
(534, 'yes', 'no', 'sdasd', 'testxxx@yopmail.com', 'dasdsa', 'adasdsa', 'adsasd', 'asdasdas', 'sdasda', 'Large Group Practice or Physician Group', '2021-10-10 06:13:41', '2021-10-10 06:13:41'),
(535, 'yes', 'no', 'sdasd', 'testxxx@yopmail.com', 'dasdsa', 'adasdsa', 'adsasd', 'asdasdas', 'sdasda', 'Large Group Practice or Physician Group', '2021-10-10 06:13:42', '2021-10-10 06:13:42'),
(536, 'yes', 'no', 'asdasd', 'testxxx@yopmail.com', 'fdfs', 'sfsdf', 'sfsdf', 'fsdfs', 'dfsdf', 'Large Group Practice or Physician Group', '2021-10-10 06:36:27', '2021-10-10 06:36:27'),
(537, 'no', 'yes', 'sdasd', 'testxxx@yopmail.com', 'sdfsd', 'asdsa', 'asdas', 'adasd', 'sadasd', 'Large Group Practice or Physician Group', '2021-10-10 06:44:34', '2021-10-10 06:44:34'),
(538, 'yes', 'no', 'sdadasda', 'testxxx@yopmail.com', 'fdfs', 'fsdfs', 'fdsfdsf', 'sfsdf', 'SFSDF', 'Large Group Practice or Physician Group', '2021-10-10 22:48:45', '2021-10-10 22:48:45'),
(539, 'yes', 'no', 'dsfsdf', 'plugin@admin.com', 'dfds', 'fdsdf', 'sdfsdds', 'sfsdf', 'sfdsdf', 'Health System', '2021-10-10 23:00:55', '2021-10-10 23:00:55'),
(540, 'yes', 'yes', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Large Group Practice or Physician Group', '2021-10-11 07:09:05', '2021-10-11 07:09:05'),
(541, 'yes', 'yes', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Large Group Practice or Physician Group', '2021-10-11 07:09:05', '2021-10-11 07:09:05'),
(542, 'yes', 'yes', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Large Group Practice or Physician Group', '2021-10-11 07:09:05', '2021-10-11 07:09:05'),
(543, 'no', 'no', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Large Group Practice or Physician Group', '2021-10-11 07:14:24', '2021-10-11 07:14:24'),
(544, 'yes', 'yes', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Test', 'Pennsylvania', NULL, 'Health System', '2021-10-12 13:33:42', '2021-10-12 13:33:42'),
(545, 'no', 'yes', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Test', 'Pennsylvania', NULL, 'Test', '2021-10-19 15:03:47', '2021-10-19 15:03:47'),
(546, 'yes', 'no', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'testing', '2021-10-20 07:42:19', '2021-10-20 07:42:19'),
(547, 'yes', 'yes', 'TEST_Kathleen', 'kathleen.kurtz@franklynhc.com', 'Editor', 'Aventria', 'Parsippany', 'NJ', NULL, 'Health System', '2021-10-22 12:17:44', '2021-10-22 12:17:44'),
(548, 'yes', 'yes', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'testing', '2021-10-26 13:19:57', '2021-10-26 13:19:57'),
(549, 'yes', 'no', 'testqwe', 'test1234test@yopmail.com', '123', 'dffdfddfdf', 'ddfddfd', 'fdfdfd', 'dfdfd', 'Large Group Practice or Physician Group', '2021-10-26 14:21:21', '2021-10-26 14:21:21'),
(550, 'yes', 'no', 'testqwe', 'test1234test@yopmail.com', '123', 'dffdfddfdf', 'ddfddfd', 'fdfdfd', 'dfdfd', 'Large Group Practice or Physician Group', '2021-10-26 14:23:09', '2021-10-26 14:23:09'),
(551, 'yes', 'yes', 'testing', 'eyesjam@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'testing', '2021-10-26 14:23:22', '2021-10-26 14:23:22'),
(552, 'yes', 'yes', 'testing', 'eyesjam@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'testing', '2021-10-26 14:23:25', '2021-10-26 14:23:25'),
(583, 'yes', 'yes', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'tezsting', '2021-10-27 07:19:27', '2021-10-27 07:19:27'),
(584, 'yes', 'no', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Test', 'Test', NULL, 'Test', '2021-11-09 15:45:20', '2021-11-09 15:45:20'),
(585, 'yes', 'no', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Test', 'Test', NULL, 'Test', '2021-11-09 15:45:21', '2021-11-09 15:45:21'),
(586, 'yes', 'no', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Test', 'Test', NULL, 'Test', '2021-11-09 15:45:24', '2021-11-09 15:45:24'),
(587, 'yes', 'no', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Test', 'Test', NULL, 'Health System', '2021-11-11 15:19:28', '2021-11-11 15:19:28'),
(590, 'yes', 'yes', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'test', '2021-11-12 10:59:16', '2021-11-12 10:59:16'),
(591, 'yes', 'yes', 'testing', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'test', '2021-11-12 10:59:19', '2021-11-12 10:59:19'),
(592, 'yes', 'yes', 'test', 'eyesyams@yopmail.com', 'test', 'test', 'test', 'test', 'test', 'test', '2021-11-12 11:44:15', '2021-11-12 11:44:15'),
(593, 'yes', 'yes', 'test', 'eyesjams@yopmail.com', 'test', 'test', 'test', 'test', 'test', 'test', '2021-11-12 11:46:03', '2021-11-12 11:46:03'),
(594, 'yes', 'no', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Test', 'Test', NULL, 'Test', '2021-11-16 13:53:08', '2021-11-16 13:53:08'),
(595, 'yes', 'yes', 'Kathleen Kurtz', 'kathleen.kurtz@franklynhc.com', 'editor', 'TEST_AVENTRIA', 'Blommfield', 'New Jersey', NULL, 'Health System', '2022-01-14 16:54:44', '2022-01-14 16:54:44'),
(596, 'yes', 'no', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Doylestown', 'Pennsylvania', 'AcctMngr Testname', 'Health System', '2022-01-18 15:50:26', '2022-01-18 15:50:26'),
(597, 'yes', 'yes', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Doylestown', 'Pennsylvania', 'AcctMngr Testname', 'Test other', '2022-01-18 17:53:36', '2022-01-18 17:53:36'),
(598, 'yes', 'yes', 'test', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Large Group Practice or Physician Group', '2022-01-19 09:30:28', '2022-01-19 09:30:28'),
(599, 'no', 'no', 'test', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Large Group Practice or Physician Group', '2022-01-19 09:31:11', '2022-01-19 09:31:11'),
(600, 'yes', 'no', 'test', 'eyesjams@yopmail.com', 'testing', 'testing', 'testing', 'testing', 'testing', 'Health System', '2022-01-20 05:37:44', '2022-01-20 05:37:44'),
(601, 'yes', 'yes', 'Katelyn Hanna-Wortley', 'k.hannawortley@gmail.com', 'Test', 'Aventria Health Group', 'Doylestown', 'Pennsylvania', 'AcctMngr Testname', 'Health System', '2022-01-20 14:33:35', '2022-01-20 14:33:35'),
(602, 'yes', 'no', 'Deb', 'deb.buhosky@aventriahealth.com', 'none', 'Aventria', 'Coopersburg', 'PA', NULL, 'cs', '2022-01-20 18:38:47', '2022-01-20 18:38:47'),
(603, 'yes', 'yes', 'test', 'eyesjam@yopmail.com', 'test', 'testt', 'test', 'test', 'test', 'Large Group Practice or Physician Group', '2022-01-21 06:03:21', '2022-01-21 06:03:21'),
(604, 'yes', 'no', 'sfsdfs', 'yoo123@yopmail.com', 'dsfsdf', 'dfdf', 'fdsfs', 'fsdf', 'dsfdsf', 'Large Group Practice or Physician Group', '2022-01-21 06:07:55', '2022-01-21 06:07:55'),
(605, 'yes', 'no', 'sdf', 'yooma@yopmail.com', 'df', 'sdfds', 'sdf', 'sdfds', 'sfdsf', 'Large Group Practice or Physician Group', '2022-01-21 06:11:52', '2022-01-21 06:11:52'),
(606, 'yes', 'no', 'sdf', 'yooma@yopmail.com', 'ads', 'dsfdss', 'dfds', 'sdfsd', 'asda', 'Large Group Practice or Physician Group', '2022-01-21 06:15:13', '2022-01-21 06:15:13'),
(607, 'yes', 'no', 'gfgghg', 'yooma@yopmail.com', 'df', 'dfdfs', 'dfdfds', 'dfds', 'sfdsf', 'Large Group Practice or Physician Group', '2022-01-21 06:17:40', '2022-01-21 06:17:40'),
(608, 'yes', 'no', 'gfgghg', 'yooma@yopmail.com', 'df', 'dfdfs', 'dfdfds', 'dfds', 'sfdsf', 'Large Group Practice or Physician Group', '2022-01-21 06:18:04', '2022-01-21 06:18:04'),
(609, 'yes', 'no', 'jhjh', 'yooma@yopmail.com', 'sdf', 'sfsd', 'sfdsf', 'sdfds', 'asda', 'Large Group Practice or Physician Group', '2022-01-21 06:21:06', '2022-01-21 06:21:06'),
(610, 'yes', 'no', 'jhjh', 'yooma@yopmail.com', 'sdf', 'sfsd', 'sfdsf', 'sdfds', 'asda', 'Large Group Practice or Physician Group', '2022-01-21 06:21:22', '2022-01-21 06:21:22'),
(611, 'no', 'yes', 'rete', 'yooma@yopmail.com', 'reter', 'erter', 'erter', 'etewrt', 'ertewr', 'Large Group Practice or Physician Group', '2022-01-21 06:30:41', '2022-01-21 06:30:41'),
(612, 'yes', 'no', 'werwe', 'yooma@yopmail.com', 'dsfd', 'fsdf', 'sdfsd', 'sdfsad', 'sdfsad', 'Large Group Practice or Physician Group', '2022-01-21 06:33:52', '2022-01-21 06:33:52'),
(613, 'no', 'yes', 'sdfsd', 'yooma@yopmail.com', 'ads', 'sfsd', 'dfdfds', 'dfds', 'sfsd', 'Health System', '2022-01-21 06:36:44', '2022-01-21 06:36:44'),
(614, 'no', 'yes', 'sdfsd', 'yooma@yopmail.com', 'ads', 'sfsd', 'dfdfds', 'dfds', 'sfsd', 'Health System', '2022-01-21 06:36:49', '2022-01-21 06:36:49'),
(615, 'no', 'yes', 'sdfs', 'yooma@yopmail.com', 'dsfsdf', 'sfsd', 'sfdsf', 'sdfds', 'asdasda', 'Large Group Practice or Physician Group', '2022-01-21 06:37:45', '2022-01-21 06:37:45'),
(616, 'no', 'yes', 'sdfs', 'yooma@yopmail.com', 'dsfsdf', 'sfsd', 'sfdsf', 'sdfds', 'asdasda', 'Large Group Practice or Physician Group', '2022-01-21 06:37:48', '2022-01-21 06:37:48'),
(617, 'no', 'yes', 'sdfs', 'yooma@yopmail.com', 'dsfsdf', 'sfsd', 'sfdsf', 'sdfds', 'asdasda', 'Large Group Practice or Physician Group', '2022-01-21 06:37:51', '2022-01-21 06:37:51'),
(618, 'yes', 'no', 'rwewe', 'yooma@yopmail.com', 'wrwer', 'wrewew', 'erwer', 'werwer', 'werwer', 'Large Group Practice or Physician Group', '2022-01-21 06:39:34', '2022-01-21 06:39:34'),
(619, 'no', 'yes', 'sfsdfs', 'yooma@yopmail.com', 'dsfsdf', 'dsf', 'dfdfds', 'sdfds', 'asda', 'Health System', '2022-01-21 06:41:52', '2022-01-21 06:41:52'),
(620, 'yes', 'no', 'df', 'yooma@yopmail.com', 'ads', 'sdf', 'sfdsf', 'sdfs', 'asda', 'Large Group Practice or Physician Group', '2022-01-21 06:44:19', '2022-01-21 06:44:19'),
(621, 'yes', 'no', 'sdfs', 'yooma@yopmail.com', 'df', 'dsf', 'dsfds', 'dfds', 'sfsd', 'Large Group Practice or Physician Group', '2022-01-21 06:46:26', '2022-01-21 06:46:26');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-04-02 00:00:01', '2020-04-02 00:00:01'),
(2, 'user', 'Normal User', '2020-04-02 00:00:01', '2020-04-02 00:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Hizentra Admin', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Admin - Hizentra QSA', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `t1`
--

CREATE TABLE `t1` (
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(3, 1, 'Hizentra Admin', 'hizentraqsa@admin.com', NULL, '$2b$10$eJR12SIfOvZVK7c3/o4EmeEzFg1Om8eGM.iX5Th1IwC7QxS7Twm/2', 'SJtW0963ElZBsXgj7sNyHu34HAlp1OwtkA4nz32TAQe5mZnBgD8zZlHCLPrc', '{\"locale\":\"en\"}', '2020-04-02 00:04:45', '2021-09-30 13:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ctas`
--
ALTER TABLE `ctas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_pages`
--
ALTER TABLE `home_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plugin_orders`
--
ALTER TABLE `plugin_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ctas`
--
ALTER TABLE `ctas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_pages`
--
ALTER TABLE `home_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `plugin_orders`
--
ALTER TABLE `plugin_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=622;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
